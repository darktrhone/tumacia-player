﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace WindowsFormsApplication2
{
    public static class MusicPlayer
    {
        [DllImport("winmm.dll")]
        private static extern int mciSendString(string strCommand, StringBuilder strReturn, int iReturnLength, IntPtr hwndCallback);

        [DllImport("winmm.dll")]
        public static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);


        internal static int Volume { get; set; }
        public static string CurrentSongName { get; set; }
        public static string CurrentArtistName { get; set; }
        public static string CurrentPlaylistName { get; set; }
        public static string CurrentSongLength { get; set; }
        public static Status IsPlaying { get; set; }


        static MusicPlayer()
        {
            Volume = 100;
            CurrentSongName = string.Empty;
            CurrentPlaylistName = string.Empty;
            CurrentArtistName = string.Empty;
            CurrentSongLength = string.Empty;
            IsPlaying =  Status.Stopped;
            SetSongVolume(Volume);
        }


        public static void VolumeUp(int vol)
        {
            //SetSongVolume(vol);
        }
        public static void VolumeDown(int vol)
        {
            
            //SetSongVolume(vol);
        }
        public static void Open(string path, string file, string pl)
        {
            
            if(string.IsNullOrWhiteSpace(path))
            path = TmcPlayer.MusicFolder;
            
            
            CurrentPlaylistName = string.IsNullOrEmpty(pl) ? "All Songs" : pl;

            CurrentSongName = file.Replace(".mp3", "");


            CurrentArtistName = TmcPlayer.GetSongArtist(CurrentPlaylistName, CurrentSongName);

            CurrentSongLength = TmcPlayer.GetSongLength(CurrentPlaylistName, CurrentSongName);



            string songnameToFile = string.IsNullOrEmpty(CurrentArtistName) ? CurrentSongName : string.Format("{0} - {1}", CurrentArtistName, CurrentSongName);

            //try
            //{
            //    File.WriteAllText(@"C:\Users\rui\Desktop\CurrentSong.txt", "            " + songnameToFile);
            //}
            //catch (Exception e)
            //{
                
            //    throw e;
            //}

            
            // MCI Song Length Broken as fuck
           // if(string.IsNullOrEmpty(CurrentSongLength))
           //     CurrentSongLength = GetSongLength();

            UserPreferences.AddCurrentSongToPlayed(file);

            if (!file.Contains(".mp3"))
                file += ".mp3";


            Stop();
            string finalPath = Path.Combine(path, file);
            mciSendString(Commands.Open(finalPath), null, 0, IntPtr.Zero);
            Play();
        }

        public static void Play(bool resume = false)
        {
            mciSendString( (resume ? Commands.Resume : Commands.Play), null, 0, IntPtr.Zero);
            IsPlaying = Status.Playing;
        }

        public static void Stop()
        {
            mciSendString(Commands.Stop, null, 0, IntPtr.Zero);
            mciSendString(Commands.Close, null, 0, IntPtr.Zero);
            IsPlaying = Status.Stopped;
        }

        public static void Pause()
        {
            mciSendString(Commands.Pause, null, 0, IntPtr.Zero);
            IsPlaying = Status.Paused;
        }

        public static void Replay()
        {
            SetCurrentPosition(0);
        }
        
        public static string CurrentPosition()
        {
            if (IsPlaying == Status.Stopped) return string.Empty;

            StringBuilder returnData = new StringBuilder(128);
            mciSendString(Commands.SongPosition, returnData, 128, IntPtr.Zero);
            return FormatTime(returnData.ToString());
        }

        public static void SetCurrentPosition(ulong milisseconds)
        {
          mciSendString(Commands.SeekTo(milisseconds), null, 0, IntPtr.Zero);
          Play();
        }
        
        public static void SetSongVolume(int newVolume)
        {
            Volume = (ushort)(newVolume * ushort.MaxValue / 100);
            var newVolumeAllChannels = ((uint)Volume & 0x0000ffff) | ((uint)Volume << 16);
            waveOutSetVolume(IntPtr.Zero, newVolumeAllChannels);
        }

        public static string GetSongVolume()
        {
            var val = Math.Round(decimal.Divide(Volume, ushort.MaxValue), 2);
            return Math.Round(val * 100).ToString();
        }

        public static int GetCurrentSongLengthInSeconds()
        {
            StringBuilder returnData = new StringBuilder(128);
            mciSendString(Commands.SongLength, returnData, returnData.Capacity, IntPtr.Zero);
            return !string.IsNullOrWhiteSpace(returnData.ToString()) ? int.Parse(returnData.ToString())/1000 : 0;

        }

        internal static string FormatTime(string returnData)
        {
            if (string.IsNullOrEmpty(returnData))
                return string.Empty;

            int seconds = int.Parse(returnData)/ 1000 ;

          return TimeSpan.FromSeconds(seconds).ToString(@"hh\:mm\:ss");
        }
        


        internal static class Commands
        {
            public static string Resume = "resume MyMp3";

            public static string Play = "play MyMp3";

            public static string Stop = "stop MyMp3";

            public static string Pause = "pause MyMp3";

            public static string Close = "close MyMp3";

            public static string SongLength ="status MyMp3 length";

            public static string SongPosition = "status MyMp3 position";

            public static string SeekTo(double mil)
            {
                return string.Format("seek MyMp3 to {0}", mil);
            }

            internal static string Open(string path)
            {
                return string.Format("open \" {0} \" type MPEGVideo alias MyMp3",path);
            }
        }


        public enum Status
        {
            Stopped,
            Playing,
            Paused
        }
        
    }

}
