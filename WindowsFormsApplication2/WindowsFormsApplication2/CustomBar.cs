﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    /// <summary>
    ///     Encapsulates control that visualy displays certain integer value and allows user to change it within desired range.
    ///     It imitates <see cref="System.Windows.Forms.TrackBar" /> as far as mouse usage is concerned.
    /// </summary>
    [ToolboxBitmap(typeof (TrackBar))]
    [DefaultEvent("Scroll"), DefaultProperty("BarInnerColor")]
    public sealed class ColorSlider : Control
    {
        #region Events

        /// <summary>
        ///     Fires when Slider position has changed
        /// </summary>
        [Description("Event fires when the Value property changes")]
        [Category("Action")]
        public event EventHandler ValueChanged;

        /// <summary>
        ///     Fires when user scrolls the Slider
        /// </summary>
        [Description("Event fires when the Slider position is changed")]
        [Category("Behavior")]
        public event ScrollEventHandler Scroll;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the thumb rect. Usefull to determine bounding rectangle when creating custom thumb shape.
        /// </summary>
        /// <value>The thumb rect.</value>
        [Browsable(false)]
        public Rectangle ThumbRect { get; private set; }

        private Rectangle _barRect; //bounding rectangle of bar area
        private Rectangle _barHalfRect;
        private Rectangle _thumbHalfRect;
        private Rectangle _elapsedRect; //bounding rectangle of elapsed area

        private int _thumbSize = 15;

        /// <summary>
        ///     Gets or sets the size of the thumb.
        /// </summary>
        /// <value>The size of the thumb.</value>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///     exception thrown when value is lower than zero or grather than
        ///     half of appropiate dimension
        /// </exception>
        [Description("Set Slider thumb size")]
        [Category("ColorSlider")]
        [DefaultValue(15)]
        public int ThumbSize
        {
            get { return _thumbSize; }
            set
            {
                if (value > 0 &
                    value < (_barOrientation == Orientation.Horizontal ? ClientRectangle.Width : ClientRectangle.Height))
                    _thumbSize = value;
                else
                    throw new ArgumentOutOfRangeException(
                        "TrackSize has to be greather than zero and lower than half of Slider width");
                Invalidate();
            }
        }

        private GraphicsPath _thumbCustomShape;

        /// <summary>
        ///     Gets or sets the thumb custom shape. Use ThumbRect property to determine bounding rectangle.
        /// </summary>
        /// <value>The thumb custom shape. null means default shape</value>
        [Description("Set Slider's thumb's custom shape")]
        [Category("ColorSlider")]
        [Browsable(false)]
        [DefaultValue(typeof (GraphicsPath), "null")]
        public GraphicsPath ThumbCustomShape
        {
            get { return _thumbCustomShape; }
            set
            {
                _thumbCustomShape = value;
                _thumbSize =
                    (int)
                        (_barOrientation == Orientation.Horizontal ? value.GetBounds().Width : value.GetBounds().Height) +
                    1;
                Invalidate();
            }
        }

        private Size _thumbRoundRectSize = new Size(8, 8);

        /// <summary>
        ///     Gets or sets the size of the thumb round rectangle edges.
        /// </summary>
        /// <value>The size of the thumb round rectangle edges.</value>
        [Description("Set Slider's thumb round rect size")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Size), "8; 8")]
        public Size ThumbRoundRectSize
        {
            get { return _thumbRoundRectSize; }
            set
            {
                int h = value.Height, w = value.Width;
                if (h <= 0) h = 1;
                if (w <= 0) w = 1;
                _thumbRoundRectSize = new Size(w, h);
                Invalidate();
            }
        }

        private Size _borderRoundRectSize = new Size(8, 8);

        /// <summary>
        ///     Gets or sets the size of the border round rect.
        /// </summary>
        /// <value>The size of the border round rect.</value>
        [Description("Set Slider's border round rect size")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Size), "8; 8")]
        public Size BorderRoundRectSize
        {
            get { return _borderRoundRectSize; }
            set
            {
                int h = value.Height, w = value.Width;
                if (h <= 0) h = 1;
                if (w <= 0) w = 1;
                _borderRoundRectSize = new Size(w, h);
                Invalidate();
            }
        }

        private Orientation _barOrientation = Orientation.Horizontal;

        /// <summary>
        ///     Gets or sets the orientation of Slider.
        /// </summary>
        /// <value>The orientation.</value>
        [Description("Set Slider orientation")]
        [Category("ColorSlider")]
        [DefaultValue(Orientation.Horizontal)]
        public Orientation Orientation
        {
            get { return _barOrientation; }
            set
            {
                if (_barOrientation != value)
                {
                    _barOrientation = value;
                    var temp = Width;
                    Width = Height;
                    Height = temp;
                    if (_thumbCustomShape != null)
                        _thumbSize =
                            (int)
                                (_barOrientation == Orientation.Horizontal
                                    ? _thumbCustomShape.GetBounds().Width
                                    : _thumbCustomShape.GetBounds().Height) + 1;
                    Invalidate();
                }
            }
        }


        private int _trackerValue = 50;

        /// <summary>
        ///     Gets or sets the value of Slider.
        /// </summary>
        /// <value>The value.</value>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///     exception thrown when value is outside appropriate range (min,
        ///     max)
        /// </exception>
        [Description("Set Slider value")]
        [Category("ColorSlider")]
        [DefaultValue(50)]
        public int Value
        {
            


            get
            {
                return _trackerValue;
            }
            set
            {
                if (value >= _barMinimum & value <= _barMaximum)
                {
                    _trackerValue = value;
                    if (ValueChanged != null) ValueChanged(this, new EventArgs());
                    Invalidate();
                }
                else throw new Exception("Erro de cenas"); 
            }
        

    }


        private int _barMinimum;

        /// <summary>
        ///     Gets or sets the minimum value.
        /// </summary>
        /// <value>The minimum value.</value>
        /// <exception cref="T:System.ArgumentOutOfRangeException">exception thrown when minimal value is greather than maximal one</exception>
        [Description("Set Slider minimal point")]
        [Category("ColorSlider")]
        [DefaultValue(0)]
        public int Minimum
        {
            get { return _barMinimum; }
            set
            {
                if (value < _barMaximum)
                {
                    _barMinimum = value;
                    if (_trackerValue < _barMinimum)
                    {
                        _trackerValue = _barMinimum;
                        if (ValueChanged != null) ValueChanged(this, new EventArgs());
                    }
                    Invalidate();
                }
                else throw new ArgumentOutOfRangeException("Minimal value is greather than maximal one");
            }
        }


        private int _barMaximum = 100;

        /// <summary>
        ///     Gets or sets the maximum value.
        /// </summary>
        /// <value>The maximum value.</value>
        /// <exception cref="T:System.ArgumentOutOfRangeException">exception thrown when maximal value is lower than minimal one</exception>
        [Description("Set Slider maximal point")]
        [Category("ColorSlider")]
        [DefaultValue(100)]
        public int Maximum
        {
            get { return _barMaximum; }
            set
            {
                if (value > _barMinimum)
                {
                    _barMaximum = value;
                    if (_trackerValue > _barMaximum)
                    {
                        _trackerValue = _barMaximum;
                        if (ValueChanged != null) ValueChanged(this, new EventArgs());
                    }
                    Invalidate();
                }
                else ;
            }
        }

        private uint _smallChange = 1;

        /// <summary>
        ///     Gets or sets trackbar's small change. It affects how to behave when directional keys are pressed
        /// </summary>
        /// <value>The small change value.</value>
        [Description("Set trackbar's small change")]
        [Category("ColorSlider")]
        [DefaultValue(1)]
        public uint SmallChange
        {
            get { return _smallChange; }
            set { _smallChange = value; }
        }

        private uint _largeChange = 5;

        /// <summary>
        ///     Gets or sets trackbar's large change. It affects how to behave when PageUp/PageDown keys are pressed
        /// </summary>
        /// <value>The large change value.</value>
        [Description("Set trackbar's large change")]
        [Category("ColorSlider")]
        [DefaultValue(5)]
        public uint LargeChange
        {
            get { return _largeChange; }
            set { _largeChange = value; }
        }

        private bool _drawFocusRectangle = true;

        /// <summary>
        ///     Gets or sets a value indicating whether to draw focus rectangle.
        /// </summary>
        /// <value><c>true</c> if focus rectangle should be drawn; otherwise, <c>false</c>.</value>
        [Description("Set whether to draw focus rectangle")]
        [Category("ColorSlider")]
        [DefaultValue(true)]
        public bool DrawFocusRectangle
        {
            get { return _drawFocusRectangle; }
            set
            {
                _drawFocusRectangle = value;
                Invalidate();
            }
        }

        private bool _drawSemitransparentThumb = true;

        /// <summary>
        ///     Gets or sets a value indicating whether to draw semitransparent thumb.
        /// </summary>
        /// <value><c>true</c> if semitransparent thumb should be drawn; otherwise, <c>false</c>.</value>
        [Description("Set whether to draw semitransparent thumb")]
        [Category("ColorSlider")]
        [DefaultValue(true)]
        public bool DrawSemitransparentThumb
        {
            get { return _drawSemitransparentThumb; }
            set
            {
                _drawSemitransparentThumb = value;
                Invalidate();
            }
        }

        private bool _mouseEffects = true;

        /// <summary>
        ///     Gets or sets whether mouse entry and exit actions have impact on how control look.
        /// </summary>
        /// <value><c>true</c> if mouse entry and exit actions have impact on how control look; otherwise, <c>false</c>.</value>
        [Description("Set whether mouse entry and exit actions have impact on how control look")]
        [Category("ColorSlider")]
        [DefaultValue(true)]
        public bool MouseEffects
        {
            get { return _mouseEffects; }
            set
            {
                _mouseEffects = value;
                Invalidate();
            }
        }

        private int _mouseWheelBarPartitions = 10;

        /// <summary>
        ///     Gets or sets the mouse wheel bar partitions.
        /// </summary>
        /// <value>The mouse wheel bar partitions.</value>
        /// <exception cref="T:System.ArgumentOutOfRangeException">exception thrown when value isn't greather than zero</exception>
        [Description("Set to how many parts is bar divided when using mouse wheel")]
        [Category("ColorSlider")]
        [DefaultValue(10)]
        public int MouseWheelBarPartitions
        {
            get { return _mouseWheelBarPartitions; }
            set
            {
                    _mouseWheelBarPartitions = value;
            }
        }

        private Color _thumbOuterColor = Color.White;

        /// <summary>
        ///     Gets or sets the thumb outer color .
        /// </summary>
        /// <value>The thumb outer color.</value>
        [Description("Set Slider thumb outer color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "White")]
        public Color ThumbOuterColor
        {
            get { return _thumbOuterColor; }
            set
            {
                _thumbOuterColor = value;
                Invalidate();
            }
        }


        private Color _thumbInnerColor = Color.Gainsboro;

        /// <summary>
        ///     Gets or sets the inner color of the thumb.
        /// </summary>
        /// <value>The inner color of the thumb.</value>
        [Description("Set Slider thumb inner color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "Gainsboro")]
        public Color ThumbInnerColor
        {
            get { return _thumbInnerColor; }
            set
            {
                _thumbInnerColor = value;
                Invalidate();
            }
        }


        private Color _thumbPenColor = Color.Silver;

        /// <summary>
        ///     Gets or sets the color of the thumb pen.
        /// </summary>
        /// <value>The color of the thumb pen.</value>
        [Description("Set Slider thumb pen color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "Silver")]
        public Color ThumbPenColor
        {
            get { return _thumbPenColor; }
            set
            {
                _thumbPenColor = value;
                Invalidate();
            }
        }


        private Color _barOuterColor = Color.SkyBlue;

        /// <summary>
        ///     Gets or sets the outer color of the bar.
        /// </summary>
        /// <value>The outer color of the bar.</value>
        [Description("Set Slider bar outer color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "SkyBlue")]
        public Color BarOuterColor
        {
            get { return _barOuterColor; }
            set
            {
                _barOuterColor = value;
                Invalidate();
            }
        }


        private Color _barInnerColor = Color.DarkSlateBlue;

        /// <summary>
        ///     Gets or sets the inner color of the bar.
        /// </summary>
        /// <value>The inner color of the bar.</value>
        [Description("Set Slider bar inner color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "DarkSlateBlue")]
        public Color BarInnerColor
        {
            get { return _barInnerColor; }
            set
            {
                _barInnerColor = value;
                Invalidate();
            }
        }


        private Color _barPenColor = Color.Gainsboro;

        /// <summary>
        ///     Gets or sets the color of the bar pen.
        /// </summary>
        /// <value>The color of the bar pen.</value>
        [Description("Set Slider bar pen color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "Gainsboro")]
        public Color BarPenColor
        {
            get { return _barPenColor; }
            set
            {
                _barPenColor = value;
                Invalidate();
            }
        }

        private Color _elapsedOuterColor = Color.DarkGreen;

        /// <summary>
        ///     Gets or sets the outer color of the elapsed.
        /// </summary>
        /// <value>The outer color of the elapsed.</value>
        [Description("Set Slider's elapsed part outer color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "DarkGreen")]
        public Color ElapsedOuterColor
        {
            get { return _elapsedOuterColor; }
            set
            {
                _elapsedOuterColor = value;
                Invalidate();
            }
        }

        private Color _elapsedInnerColor = Color.Chartreuse;

        /// <summary>
        ///     Gets or sets the inner color of the elapsed.
        /// </summary>
        /// <value>The inner color of the elapsed.</value>
        [Description("Set Slider's elapsed part inner color")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (Color), "Chartreuse")]
        public Color ElapsedInnerColor
        {
            get { return _elapsedInnerColor; }
            set
            {
                _elapsedInnerColor = value;
                Invalidate();
            }
        }

        #endregion

        #region Color schemas

        //define own color schemas
        private readonly Color[,] _aColorSchema =
        {
            {
                Color.White, Color.Gainsboro, Color.Silver, Color.SkyBlue, Color.DarkSlateBlue, Color.Gainsboro,
                Color.DarkGreen, Color.Chartreuse
            },
            {
                Color.White, Color.Gainsboro, Color.Silver, Color.Red, Color.DarkRed, Color.Gainsboro, Color.Coral,
                Color.LightCoral
            },
            {
                Color.White, Color.Gainsboro, Color.Silver, Color.GreenYellow, Color.Yellow, Color.Gold, Color.Orange,
                Color.OrangeRed
            },
            {
                Color.White, Color.Gainsboro, Color.Silver, Color.Red, Color.Crimson, Color.Gainsboro, Color.DarkViolet
                , Color.Violet
            }
        };

        public enum ColorSchemas
        {
            PerlBlueGreen,
            PerlRedCoral,
            PerlGold,
            PerlRoyalColors
        }

        private ColorSchemas _colorSchema = ColorSchemas.PerlBlueGreen;

        /// <summary>
        ///     Sets color schema. Color generalization / fast color changing. Has no effect when slider colors are changed
        ///     manually after schema was applied.
        /// </summary>
        /// <value>New color schema value</value>
        [Description(
            "Set Slider color schema. Has no effect when slider colors are changed manually after schema was applied.")]
        [Category("ColorSlider")]
        [DefaultValue(typeof (ColorSchemas), "PerlBlueGreen")]
        public ColorSchemas ColorSchema
        {
            get { return _colorSchema; }
            set
            {
                _colorSchema = value;
                var sn = (byte) value;
                _thumbOuterColor = _aColorSchema[sn, 0];
                _thumbInnerColor = _aColorSchema[sn, 1];
                _thumbPenColor = _aColorSchema[sn, 2];
                _barOuterColor = _aColorSchema[sn, 3];
                _barInnerColor = _aColorSchema[sn, 4];
                _barPenColor = _aColorSchema[sn, 5];
                _elapsedOuterColor = _aColorSchema[sn, 6];
                _elapsedInnerColor = _aColorSchema[sn, 7];

                Invalidate();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColorSlider" /> class.
        /// </summary>
        /// <param name="min">The minimum value.</param>
        /// <param name="max">The maximum value.</param>
        /// <param name="value">The current value.</param>
        public ColorSlider(int min, int max, int value)
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer |
                     ControlStyles.ResizeRedraw | ControlStyles.Selectable |
                     ControlStyles.SupportsTransparentBackColor | ControlStyles.UserMouse |
                     ControlStyles.UserPaint, true);
            BackColor = Color.Transparent;

            Minimum = min;
            Maximum = max;
            Value = value;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColorSlider" /> class.
        /// </summary>
        public ColorSlider() : this(0, 100, 50)
        {
        }

        #endregion

        #region Paint

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.Paint"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs"></see> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            if (!Enabled)
            {
                var desaturatedColors = DesaturateColors(_thumbOuterColor, _thumbInnerColor, _thumbPenColor,
                    _barOuterColor, _barInnerColor, _barPenColor,
                    _elapsedOuterColor, _elapsedInnerColor);
                DrawColorSlider(e, desaturatedColors[0], desaturatedColors[1], desaturatedColors[2],
                    desaturatedColors[3],
                    desaturatedColors[4], desaturatedColors[5], desaturatedColors[6], desaturatedColors[7]);
            }
            else
            {
                if (_mouseEffects && _mouseInRegion)
                {
                    var lightenedColors = LightenColors(_thumbOuterColor, _thumbInnerColor, _thumbPenColor,
                        _barOuterColor, _barInnerColor, _barPenColor,
                        _elapsedOuterColor, _elapsedInnerColor);
                    DrawColorSlider(e, lightenedColors[0], lightenedColors[1], lightenedColors[2], lightenedColors[3],
                        lightenedColors[4], lightenedColors[5], lightenedColors[6], lightenedColors[7]);
                }
                else
                {
                    DrawColorSlider(e, _thumbOuterColor, _thumbInnerColor, _thumbPenColor,
                        _barOuterColor, _barInnerColor, _barPenColor,
                        _elapsedOuterColor, _elapsedInnerColor);
                }
            }
        }

        /// <summary>
        ///     Draws the colorslider control using passed colors.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Forms.PaintEventArgs" /> instance containing the event data.</param>
        /// <param name="thumbOuterColorPaint">The thumb outer color paint.</param>
        /// <param name="thumbInnerColorPaint">The thumb inner color paint.</param>
        /// <param name="thumbPenColorPaint">The thumb pen color paint.</param>
        /// <param name="barOuterColorPaint">The bar outer color paint.</param>
        /// <param name="barInnerColorPaint">The bar inner color paint.</param>
        /// <param name="barPenColorPaint">The bar pen color paint.</param>
        /// <param name="elapsedOuterColorPaint">The elapsed outer color paint.</param>
        /// <param name="elapsedInnerColorPaint">The elapsed inner color paint.</param>
        private void DrawColorSlider(PaintEventArgs e, Color thumbOuterColorPaint, Color thumbInnerColorPaint,
            Color thumbPenColorPaint, Color barOuterColorPaint, Color barInnerColorPaint,
            Color barPenColorPaint, Color elapsedOuterColorPaint, Color elapsedInnerColorPaint)
        {
            try
            {
                //set up thumbRect aproprietly
                if (_barOrientation == Orientation.Horizontal)
                {
                    var trackX = (((_trackerValue - _barMinimum)*(ClientRectangle.Width - _thumbSize))/
                                  (_barMaximum - _barMinimum));
                    ThumbRect = new Rectangle(trackX, 1, _thumbSize - 1, ClientRectangle.Height - 3);
                }
                else
                {
                    var trackY = (((_trackerValue - _barMinimum)*(ClientRectangle.Height - _thumbSize))/
                                  (_barMaximum - _barMinimum));
                    ThumbRect = new Rectangle(1, trackY, ClientRectangle.Width - 3, _thumbSize - 1);
                }

                //adjust drawing rects
                _barRect = ClientRectangle;
                _thumbHalfRect = ThumbRect;
                LinearGradientMode gradientOrientation;
                if (_barOrientation == Orientation.Horizontal)
                {
                    _barRect.Inflate(-1, -_barRect.Height/3);
                    _barHalfRect = _barRect;
                    _barHalfRect.Height /= 2;
                    gradientOrientation = LinearGradientMode.Vertical;
                    _thumbHalfRect.Height /= 2;
                    _elapsedRect = _barRect;
                    _elapsedRect.Width = ThumbRect.Left + _thumbSize/2;
                }
                else
                {
                    _barRect.Inflate(-_barRect.Width/3, -1);
                    _barHalfRect = _barRect;
                    _barHalfRect.Width /= 2;
                    gradientOrientation = LinearGradientMode.Horizontal;
                    _thumbHalfRect.Width /= 2;
                    _elapsedRect = _barRect;
                    _elapsedRect.Height = ThumbRect.Top + _thumbSize/2;
                }
                //get thumb shape path 
                GraphicsPath thumbPath;
                if (_thumbCustomShape == null)
                    thumbPath = CreateRoundRectPath(ThumbRect, _thumbRoundRectSize);
                else
                {
                    thumbPath = _thumbCustomShape;
                    var m = new Matrix();
                    m.Translate(ThumbRect.Left - thumbPath.GetBounds().Left, ThumbRect.Top - thumbPath.GetBounds().Top);
                    thumbPath.Transform(m);
                }

                //draw bar
                using (
                    var lgbBar =
                        new LinearGradientBrush(_barHalfRect, barOuterColorPaint, barInnerColorPaint,
                            gradientOrientation)
                    )
                {
                    lgbBar.WrapMode = WrapMode.TileFlipXY;
                    e.Graphics.FillRectangle(lgbBar, _barRect);
                    //draw elapsed bar
                    using (
                        var lgbElapsed =
                            new LinearGradientBrush(_barHalfRect, elapsedOuterColorPaint, elapsedInnerColorPaint,
                                gradientOrientation))
                    {
                        lgbElapsed.WrapMode = WrapMode.TileFlipXY;
                        if (Capture && _drawSemitransparentThumb)
                        {
                            var elapsedReg = new Region(_elapsedRect);
                            elapsedReg.Exclude(thumbPath);
                            e.Graphics.FillRegion(lgbElapsed, elapsedReg);
                        }
                        else
                            e.Graphics.FillRectangle(lgbElapsed, _elapsedRect);
                    }
                    //draw bar band                    
                    using (var barPen = new Pen(barPenColorPaint, 0.5f))
                    {
                        e.Graphics.DrawRectangle(barPen, _barRect);
                    }
                }

                //draw thumb
                Color newthumbOuterColorPaint = thumbOuterColorPaint, newthumbInnerColorPaint = thumbInnerColorPaint;
                if (Capture && _drawSemitransparentThumb)
                {
                    newthumbOuterColorPaint = Color.FromArgb(175, thumbOuterColorPaint);
                    newthumbInnerColorPaint = Color.FromArgb(175, thumbInnerColorPaint);
                }
                using (
                    var lgbThumb =
                        new LinearGradientBrush(_thumbHalfRect, newthumbOuterColorPaint, newthumbInnerColorPaint,
                            gradientOrientation))
                {
                    lgbThumb.WrapMode = WrapMode.TileFlipXY;
                    e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    e.Graphics.FillPath(lgbThumb, thumbPath);
                    //draw thumb band
                    var newThumbPenColor = thumbPenColorPaint;
                    if (_mouseEffects && (Capture || _mouseInThumbRegion))
                        newThumbPenColor = ControlPaint.Dark(newThumbPenColor);
                    using (var thumbPen = new Pen(newThumbPenColor))
                    {
                        e.Graphics.DrawPath(thumbPen, thumbPath);
                    }
                    //gp.Dispose();                    
                    /*if (Capture || mouseInThumbRegion)
                        using (LinearGradientBrush lgbThumb2 = new LinearGradientBrush(thumbHalfRect, Color.FromArgb(150, Color.Blue), Color.Transparent, gradientOrientation))
                        {
                            lgbThumb2.WrapMode = WrapMode.TileFlipXY;
                            e.Graphics.FillPath(lgbThumb2, gp);
                        }*/
                }

                //draw focusing rectangle
                if (Focused & _drawFocusRectangle)
                    using (var p = new Pen(Color.FromArgb(200, barPenColorPaint)))
                    {
                        p.DashStyle = DashStyle.Dot;
                        var r = ClientRectangle;
                        r.Width -= 2;
                        r.Height--;
                        r.X++;
                        //ControlPaint.DrawFocusRectangle(e.Graphics, r);                        
                        using (var gpBorder = CreateRoundRectPath(r, _borderRoundRectSize))
                        {
                            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                            e.Graphics.DrawPath(p, gpBorder);
                        }
                    }
            }
            catch (Exception err)
            {
                Console.WriteLine("DrawBackGround Error in " + Name + ":" + err.Message);
            }
        }

        #endregion

        #region Overided events

        private bool _mouseInRegion;

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.EnabledChanged"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.MouseEnter"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            _mouseInRegion = true;
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            _mouseInRegion = false;
            _mouseInThumbRegion = false;
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.MouseDown"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs"></see> that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                Capture = true;
                if (Scroll != null) Scroll(this, new ScrollEventArgs(ScrollEventType.ThumbTrack, _trackerValue));
                if (ValueChanged != null) ValueChanged(this, new EventArgs());
                OnMouseMove(e);
            }
        }

        private bool _mouseInThumbRegion;

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.MouseMove"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs"></see> that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            _mouseInThumbRegion = IsPointInRect(e.Location, ThumbRect);
            if (Capture & e.Button == MouseButtons.Left)
            {
                var set = ScrollEventType.ThumbPosition;
                var pt = e.Location;
                var p = _barOrientation == Orientation.Horizontal ? pt.X : pt.Y;
                var margin = _thumbSize >> 1;
                p -= margin;
                var coef = (_barMaximum - _barMinimum)/
                           (float)
                               ((_barOrientation == Orientation.Horizontal ? ClientSize.Width : ClientSize.Height) -
                                2*margin);
                _trackerValue = (int) (p*coef + _barMinimum);

                if (_trackerValue <= _barMinimum)
                {
                    _trackerValue = _barMinimum;
                    set = ScrollEventType.First;
                }
                else if (_trackerValue >= _barMaximum)
                {
                    _trackerValue = _barMaximum;
                    set = ScrollEventType.Last;
                }

                if (Scroll != null) Scroll(this, new ScrollEventArgs(set, _trackerValue));
                if (ValueChanged != null) ValueChanged(this, new EventArgs());
            }
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.MouseUp"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs"></see> that contains the event data.</param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            Capture = false;
            _mouseInThumbRegion = IsPointInRect(e.Location, ThumbRect);
            if (Scroll != null) Scroll(this, new ScrollEventArgs(ScrollEventType.EndScroll, _trackerValue));
            if (ValueChanged != null) ValueChanged(this, new EventArgs());
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.MouseWheel"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs"></see> that contains the event data.</param>
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            return;
            base.OnMouseWheel(e);
            var v = e.Delta/120*(_barMaximum - _barMinimum)/_mouseWheelBarPartitions;
            SetProperValue(Value + v);
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.GotFocus"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.LostFocus"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            Invalidate();
        }

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Forms.Control.KeyUp"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.KeyEventArgs"></see> that contains the event data.</param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            switch (e.KeyCode)
            {
                case Keys.Down:
                case Keys.Left:
                    SetProperValue(Value - (int) _smallChange);
                    if (Scroll != null) Scroll(this, new ScrollEventArgs(ScrollEventType.SmallDecrement, Value));
                    break;
                case Keys.Up:
                case Keys.Right:
                    SetProperValue(Value + (int) _smallChange);
                    if (Scroll != null) Scroll(this, new ScrollEventArgs(ScrollEventType.SmallIncrement, Value));
                    break;
                case Keys.Home:
                    Value = _barMinimum;
                    break;
                case Keys.End:
                    Value = _barMaximum;
                    break;
                case Keys.PageDown:
                    SetProperValue(Value - (int) _largeChange);
                    if (Scroll != null) Scroll(this, new ScrollEventArgs(ScrollEventType.LargeDecrement, Value));
                    break;
                case Keys.PageUp:
                    SetProperValue(Value + (int) _largeChange);
                    if (Scroll != null) Scroll(this, new ScrollEventArgs(ScrollEventType.LargeIncrement, Value));
                    break;
            }
            if (Scroll != null && Value == _barMinimum) Scroll(this, new ScrollEventArgs(ScrollEventType.First, Value));
            if (Scroll != null && Value == _barMaximum) Scroll(this, new ScrollEventArgs(ScrollEventType.Last, Value));
            var pt = PointToClient(Cursor.Position);
            OnMouseMove(new MouseEventArgs(MouseButtons.None, 0, pt.X, pt.Y, 0));
        }

        /// <summary>
        ///     Processes a dialog key.
        /// </summary>
        /// <param name="keyData">
        ///     One of the <see cref="T:System.Windows.Forms.Keys"></see> values that represents the key to
        ///     process.
        /// </param>
        /// <returns>
        ///     true if the key was processed by the control; otherwise, false.
        /// </returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Tab | ModifierKeys == Keys.Shift)
                return base.ProcessDialogKey(keyData);
            OnKeyDown(new KeyEventArgs(keyData));
            return true;
        }

        #endregion

        #region Help routines

        /// <summary>
        ///     Creates the round rect path.
        /// </summary>
        /// <param name="rect">The rectangle on which graphics path will be spanned.</param>
        /// <param name="size">The size of rounded rectangle edges.</param>
        /// <returns></returns>
        public static GraphicsPath CreateRoundRectPath(Rectangle rect, Size size)
        {
            var gp = new GraphicsPath();
            gp.AddLine(rect.Left + size.Width/2, rect.Top, rect.Right - size.Width/2, rect.Top);
            gp.AddArc(rect.Right - size.Width, rect.Top, size.Width, size.Height, 270, 90);

            gp.AddLine(rect.Right, rect.Top + size.Height/2, rect.Right, rect.Bottom - size.Width/2);
            gp.AddArc(rect.Right - size.Width, rect.Bottom - size.Height, size.Width, size.Height, 0, 90);

            gp.AddLine(rect.Right - size.Width/2, rect.Bottom, rect.Left + size.Width/2, rect.Bottom);
            gp.AddArc(rect.Left, rect.Bottom - size.Height, size.Width, size.Height, 90, 90);

            gp.AddLine(rect.Left, rect.Bottom - size.Height/2, rect.Left, rect.Top + size.Height/2);
            gp.AddArc(rect.Left, rect.Top, size.Width, size.Height, 180, 90);
            return gp;
        }

        /// <summary>
        ///     Desaturates colors from given array.
        /// </summary>
        /// <param name="colorsToDesaturate">The colors to be desaturated.</param>
        /// <returns></returns>
        public static Color[] DesaturateColors(params Color[] colorsToDesaturate)
        {
            var colorsToReturn = new Color[colorsToDesaturate.Length];
            for (var i = 0; i < colorsToDesaturate.Length; i++)
            {
                //use NTSC weighted avarage
                var gray =
                    (int) (colorsToDesaturate[i].R*0.3 + colorsToDesaturate[i].G*0.6 + colorsToDesaturate[i].B*0.1);
                colorsToReturn[i] = Color.FromArgb(-0x010101*(255 - gray) - 1);
            }
            return colorsToReturn;
        }

        /// <summary>
        ///     Lightens colors from given array.
        /// </summary>
        /// <param name="colorsToLighten">The colors to lighten.</param>
        /// <returns></returns>
        public static Color[] LightenColors(params Color[] colorsToLighten)
        {
            var colorsToReturn = new Color[colorsToLighten.Length];
            for (var i = 0; i < colorsToLighten.Length; i++)
            {
                colorsToReturn[i] = ControlPaint.Light(colorsToLighten[i]);
            }
            return colorsToReturn;
        }

        /// <summary>
        ///     Sets the trackbar value so that it wont exceed allowed range.
        /// </summary>
        /// <param name="val">The value.</param>
        private void SetProperValue(int val)
        {
            if (val < _barMinimum) Value = _barMinimum;
            else if (val > _barMaximum) Value = _barMaximum;
            else Value = val;
        }

        /// <summary>
        ///     Determines whether rectangle contains given point.
        /// </summary>
        /// <param name="pt">The point to test.</param>
        /// <param name="rect">The base rectangle.</param>
        /// <returns>
        ///     <c>true</c> if rectangle contains given point; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsPointInRect(Point pt, Rectangle rect)
        {
            return pt.X > rect.Left & pt.X < rect.Right & pt.Y > rect.Top & pt.Y < rect.Bottom;
        }

        #endregion
    }
}