﻿using System.Collections.Generic;
using System.Linq;


namespace WindowsFormsApplication2
{
    static class UserPreferences
    {

        static UserPreferences()
        {
            PopulateUserPreferences();

        }

        private static void PopulateUserPreferences()
        {
            SortDirection = string.Empty;
            SongsPlayed = new List<Song>();
            SongsToPlayShuffle = new List<Song>();
            SongsToReplay = new List<Song>();
            IsShuffle = false;
            IsRepeat = false;
            SelectedSongIndex = 0;
            ListOfPlaylist = TmcPlayer.Library.Select(x => x.Name).ToList();


            ListOfArtists = TmcPlayer.Library
              .SelectMany(i => i.PlayListSongs)
              .Where(a => !string.IsNullOrEmpty(a.Artist))
              .Select(e => e.Artist)
              .Distinct()
              .ToList();
        }


        public static string SelectedSong { get; set; }
        public static int SelectedSongIndex { get; set; }
        public static string SelectedContextMenuPlaylist { get; set; }

        public static string SelectedPlaylist { get; set; }


        public static string SortDirection { get; set; }
        public static List<string> ListOfArtists { get; set; }
        public static List<string> ListOfPlaylist { get; set; }

        public static bool IsShuffle { get; set; }
        public static bool IsRepeat { get; set; }

        public static List<Song> SongsToPlayShuffle { get; set; }

        public static List<Song> SongsPlayed { get; set; }

        //lista que tem músicas que já foram tocadas mas que entretanto o user fez retroceder e tem que tocar novamente
        public static List<Song> SongsToReplay { get; set; }
        public static void ClearPlayedSongs()
        {
            SongsPlayed.Clear();
        }

        public static void AddCurrentSongToPlayed(string songname = "")
        {



            if (!string.IsNullOrEmpty(songname))
            {
                if (IsShuffle)
                    SongsToPlayShuffle.RemoveAll(x => x.Name == songname);

                SongsPlayed.Add(new Song(songname));
                return;
            }



            if (SongsPlayed.Exists(s => s.ToString() == SelectedSong))
                return;


            if (IsShuffle)
                SongsToPlayShuffle.RemoveAll(x => x.Name == SelectedSong);

            SongsPlayed.Add(new Song(SelectedSong));


        }

        public static void RemoveSongPlayedAtIndex(int index)
        {
            SongsPlayed.RemoveAt(index);
        }

        public static int GetMusicPlayedCount()
        {
            return SongsPlayed.Count;
        }


        public static Song GetSongAtIndex(int index)
        {
            return SongsPlayed[index];
        }

        public static bool SongWasPlayedBefore(string songname)
        {
            return SongsPlayed.Exists(x => x.ToString() == songname);
        }

        public static void RemoveFirstFromReplayedSongs()
        {
            SongsToReplay.RemoveAt(SongsToReplay.Count-1);
        }
        public static void RemoveFromShuffleSongs(int index)
        {
            SongsToPlayShuffle.RemoveAt(index);
        }


    }
}
