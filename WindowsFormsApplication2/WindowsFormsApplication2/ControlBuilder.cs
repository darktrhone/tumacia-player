﻿using System.Drawing;
using System.Windows.Forms;
using WindowsFormsApplication2.Properties;
namespace WindowsFormsApplication2
{
    static class ControlBuilder
    {

        public static Label CreateArtistPanelForSongs(string text, int x)
        {
            /*
            Panel p = new Panel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left,
                Location = new Point(234-x, 4),
                MaximumSize = new Size(470, 20),
                Size = new Size(450, 20),
                MinimumSize = new Size(186,20)
            };
             * */

            /*
            p.Controls.Add(new MetroLabel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Left,
                Text = text,
                ForeColor = Color.Chocolate,
                Size = new Size(180, 20),
                Location = new Point(280, 9),
                FontWeight = MetroLabelWeight.Bold,
                FontSize = MetroLabelSize.Small,
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter,
                UseCustomBackColor = true,
                UseCustomForeColor = true,
                Dock = DockStyle.Right,
                Enabled = false,
            });
            */

            return new Label
            {
                Text = text,
                ForeColor = Color.Chocolate,
                Font = new Font("Tahoma", 8F, FontStyle.Bold),
                Size = new Size(180, 20),
                Location = new Point(280, 7),
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter
            };
        }

        public static Label CreateAlbumPanelForSongs(string text, int x)
        {
            /*
            Panel p = new Panel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left,
                Location = new Point(490-x, 4),
                MaximumSize = new Size(470, 20),
                Size = new Size(440, 20),
                MinimumSize = new Size(150, 20)
            };
             */

            /*
            p.Controls.Add(new MetroLabel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Left,
                Text = text,
                ForeColor = Color.Chocolate,
                Size = new Size(150, 20),
                Location = new Point(490, 9),
                FontWeight = MetroLabelWeight.Bold,
                FontSize = MetroLabelSize.Small,
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter,
                UseCustomBackColor = true,
                UseCustomForeColor = true,
                Dock = DockStyle.Right,
                Enabled = false,
            });
             * */
         

            return new Label
            {
                Text = text,
                ForeColor = Color.Chocolate,
                Font = new Font("Tahoma", 8F, FontStyle.Bold),
                Size = new Size(150, 20),
                Location = new Point(490, 6),
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter
            }; 

            //);

           // return p;
            // return p;
        }
        
        public static Label CreateHorizontalRowForSongs(int x)
        {
            return new Label
            {
                Size = new Size(1115, 1),
                MinimumSize = new Size(500,0),
                Location = new Point(0-x, 30),
                BackColor = Color.CadetBlue,
            };
        }


        public static Label CreateDurationPanelForSongs(string text, int x)
        {
            /*
            return 
            */
            /*
            Panel p = new Panel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Right,
                Location = new Point(1025-x, 4),
                MaximumSize = new Size(150, 20),
                Size = new Size(74, 20)
            };
            */
/*
            p.Controls.Add(new MetroLabel
            {
                Text = text,
                ForeColor = Color.Chocolate,
                Size = new Size(70, 20),
                Location = new Point(690, 9),
                FontWeight = MetroLabelWeight.Bold,
                FontSize = MetroLabelSize.Small,
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter,
                UseCustomBackColor = true,
                UseCustomForeColor = true,
                Dock = DockStyle.Fill,
                Enabled = false,
            });
            */

            return new Label
            {
                Text = text,
                ForeColor = Color.Chocolate,
                Font = new Font("Tahoma", 8F, FontStyle.Bold),
                Size = new Size(70, 20),
                Location = new Point(690, 6),
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter
            };


        }

        public static Label CreateNamePanelForSongs(string text, int x)
        {
            /*
            Panel p = new Panel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left,
                Location = new Point(40-x, 7),
                MaximumSize = new Size(372, 20),
                Size = new Size(352, 15),
                MinimumSize = new Size(213,15)
            };
            /*
            p.Controls.Add(new MetroLabel
            {
                Anchor = AnchorStyles.Top | AnchorStyles.Left,
                Text = text,
                ForeColor = Color.Chocolate,
                Size = new Size(200, 20),
                Location = new Point(45, 4),
                FontWeight = MetroLabelWeight.Bold,
                FontSize = MetroLabelSize.Small,
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter,
                UseCustomBackColor = true,
                UseCustomForeColor = true,
                Dock = DockStyle.Right,
                Enabled = false,
            });
             * */

            return new Label
            {
                Text = text,
                ForeColor = Color.Chocolate,
                Font = new Font("Tahoma", 8F, FontStyle.Bold),
                Size = new Size(200, 13),
                Location = new Point(45, 11),
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter
            };



        }



        public static Label CreateIndexLabelInsidePanelForSongs(string text)
        {
            return new Label
            {
                Text = text,
                ForeColor = Color.Firebrick,
                BorderStyle = BorderStyle.None,
                Font = new Font("Tahoma", 8F, FontStyle.Bold),
                Cursor = Cursors.Hand,
                Size = new Size(30, 20),
                Location = new Point(27, 7),
                BackColor = Color.Transparent,
                TextAlign = ContentAlignment.MiddleCenter
            };
        }



        public static ColorSlider CreateSliderVolume(int x, int y)
        {
            return new ColorSlider
            {
                Width = 65,
                Height = 17,
                BarInnerColor = Color.Black,
                ElapsedInnerColor = Color.ForestGreen,
                ElapsedOuterColor = Color.Transparent,
                SmallChange = 1,
                Maximum = 100,
                Minimum = 0,
                Value = 100,
                MouseEffects = true,
                Location = new Point(x + 20, y),
                MouseWheelBarPartitions = 0
            };
        }


        public static ColorSlider CreatePositionSlider()
        {
           return new ColorSlider
            {
                Width = 350,
                Height = 20,
                BarInnerColor = Color.Black,
                ElapsedInnerColor = Color.Peru,
                ElapsedOuterColor = Color.PaleVioletRed,
                SmallChange = 1,
                LargeChange = 5,
                Maximum = 1000,
                Minimum = 0,
                Value = 0,
                MouseEffects = true,
                MouseWheelBarPartitions = 0
            };
        }

        public static TextBox CreateTextBoxNewPlaylist()
        {
           return new TextBox
            {
                Name = "tbPlaylistToAdd",
                Location = new Point(1, 2 + 19 * TmcPlayer.Library.Count),
                BackColor = Color.LimeGreen,
                BorderStyle = BorderStyle.None,
                Font = new Font("Microsoft Sans Serif", 10F),
                MaxLength = 30,
                Size = new Size(213, 21),
                Text = "Playlist",
                TextAlign = HorizontalAlignment.Center
            };
        }

        internal static Panel CreatePanelForSongs(string songName, int y)
        {
            return new Panel
            {
                Cursor = Cursors.Hand,
                Size = new Size(782, 31),
                AutoScroll = false,
                Margin = new Padding(0),
                Location = new Point(0, y),
                Tag = songName
            };

        }

        public static Control CreatePbImageAddToFavourite(Song song)
        {
            var p =  new PictureBox
            {
                Image = (song.IsFavourite == 1) ? Resources.GoldStar : Resources.WhiteStar,
                Size = new Size(16, 16),
                Location = new Point(4, 8),
                SizeMode = PictureBoxSizeMode.StretchImage,
                Tag = song.Name
            };
            p.Click += (s, e) =>
            {
                UserPreferences.SelectedSong = song.Name;
                TmcPlayer.SetSongAsFavourite(song.Name, UserPreferences.SelectedPlaylist);
                p.Image = TmcPlayer.IsSongFavourite(UserPreferences.SelectedPlaylist, song.Name) ? Resources.GoldStar : Resources.WhiteStar;
            };
            return p;
        }
    }
}
