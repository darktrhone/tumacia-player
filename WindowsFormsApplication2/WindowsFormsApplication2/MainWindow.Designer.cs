﻿using System.ComponentModel;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    partial class MainWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindowForm));
            this.PanelMainBottom = new System.Windows.Forms.Panel();
            this.lbCurrentTime = new System.Windows.Forms.Label();
            this.panelSongPosition = new System.Windows.Forms.Panel();
            this.PanelBottomLeftUserControls = new System.Windows.Forms.Panel();
            this.lbCurrentArtist = new System.Windows.Forms.Label();
            this.pbAvancar = new System.Windows.Forms.PictureBox();
            this.pbRetroceder = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbCurrentSong = new System.Windows.Forms.Label();
            this.pbPlayButton = new System.Windows.Forms.PictureBox();
            this.pbSpeaker = new System.Windows.Forms.PictureBox();
            this.lbVolume = new System.Windows.Forms.Label();
            this.pbFav = new System.Windows.Forms.PictureBox();
            this.pbShuffle = new System.Windows.Forms.PictureBox();
            this.pbReplay = new System.Windows.Forms.PictureBox();
            this.lbDuration = new System.Windows.Forms.Label();
            this.panelContainerForUserSongs = new System.Windows.Forms.Panel();
            this.PanelPubAndEditSong = new System.Windows.Forms.Panel();
            this.PanelEditSongs = new System.Windows.Forms.Panel();
            this.tbEditArtist = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbAddArtist = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbEditAlbum = new System.Windows.Forms.TextBox();
            this.btnApplyChanges = new System.Windows.Forms.Button();
            this.cbEditArtist = new MetroFramework.Controls.MetroComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEditSongname = new System.Windows.Forms.TextBox();
            this.PanelCenter = new System.Windows.Forms.Panel();
            this.panelFieldIndicators = new System.Windows.Forms.Panel();
            this.lblOrderByDuration = new System.Windows.Forms.Label();
            this.lblOrderByAlbum = new System.Windows.Forms.Label();
            this.lblOrderByArtist = new System.Windows.Forms.Label();
            this.lblOrderByName = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblOrderByEntry = new System.Windows.Forms.Label();
            this.pbDefaultStarOrder = new System.Windows.Forms.PictureBox();
            this.panelSongsContainer = new System.Windows.Forms.Panel();
            this.panelLeftBlockUserPlaylistsLeft = new System.Windows.Forms.Panel();
            this.lbAddNewPlaylist = new MetroFramework.Controls.MetroLabel();
            this.PanelUserPlaylist = new System.Windows.Forms.Panel();
            this.panelNavigator = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.PanelMainTop = new System.Windows.Forms.Panel();
            this.pbMinimize = new System.Windows.Forms.PictureBox();
            this.pbMaximize = new System.Windows.Forms.PictureBox();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panelMainCenterContainer = new System.Windows.Forms.Panel();
            this.SongsContextMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.tsmPlay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAddTo = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eraseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tdmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.label6 = new System.Windows.Forms.Label();
            this.PlaylistContextMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.playToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelMainBottom.SuspendLayout();
            this.PanelBottomLeftUserControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAvancar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRetroceder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpeaker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFav)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbShuffle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReplay)).BeginInit();
            this.panelContainerForUserSongs.SuspendLayout();
            this.PanelPubAndEditSong.SuspendLayout();
            this.PanelEditSongs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddArtist)).BeginInit();
            this.PanelCenter.SuspendLayout();
            this.panelFieldIndicators.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDefaultStarOrder)).BeginInit();
            this.panelLeftBlockUserPlaylistsLeft.SuspendLayout();
            this.panelNavigator.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelSearch.SuspendLayout();
            this.PanelMainTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            this.panel5.SuspendLayout();
            this.panelMainCenterContainer.SuspendLayout();
            this.SongsContextMenu.SuspendLayout();
            this.PlaylistContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelMainBottom
            // 
            this.PanelMainBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelMainBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelMainBottom.Controls.Add(this.lbCurrentTime);
            this.PanelMainBottom.Controls.Add(this.panelSongPosition);
            this.PanelMainBottom.Controls.Add(this.PanelBottomLeftUserControls);
            this.PanelMainBottom.Controls.Add(this.pbFav);
            this.PanelMainBottom.Controls.Add(this.pbShuffle);
            this.PanelMainBottom.Controls.Add(this.pbReplay);
            this.PanelMainBottom.Controls.Add(this.lbDuration);
            this.PanelMainBottom.Location = new System.Drawing.Point(3, 620);
            this.PanelMainBottom.Name = "PanelMainBottom";
            this.PanelMainBottom.Size = new System.Drawing.Size(1018, 97);
            this.PanelMainBottom.TabIndex = 13;
            // 
            // lbCurrentTime
            // 
            this.lbCurrentTime.AutoSize = true;
            this.lbCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbCurrentTime.Location = new System.Drawing.Point(366, 64);
            this.lbCurrentTime.Name = "lbCurrentTime";
            this.lbCurrentTime.Size = new System.Drawing.Size(38, 15);
            this.lbCurrentTime.TabIndex = 24;
            this.lbCurrentTime.Text = "00:00";
            // 
            // panelSongPosition
            // 
            this.panelSongPosition.Location = new System.Drawing.Point(410, 62);
            this.panelSongPosition.Name = "panelSongPosition";
            this.panelSongPosition.Size = new System.Drawing.Size(361, 22);
            this.panelSongPosition.TabIndex = 23;
            // 
            // PanelBottomLeftUserControls
            // 
            this.PanelBottomLeftUserControls.Controls.Add(this.lbCurrentArtist);
            this.PanelBottomLeftUserControls.Controls.Add(this.pbAvancar);
            this.PanelBottomLeftUserControls.Controls.Add(this.pbRetroceder);
            this.PanelBottomLeftUserControls.Controls.Add(this.label2);
            this.PanelBottomLeftUserControls.Controls.Add(this.lbCurrentSong);
            this.PanelBottomLeftUserControls.Controls.Add(this.pbPlayButton);
            this.PanelBottomLeftUserControls.Controls.Add(this.pbSpeaker);
            this.PanelBottomLeftUserControls.Controls.Add(this.lbVolume);
            this.PanelBottomLeftUserControls.Location = new System.Drawing.Point(1, 1);
            this.PanelBottomLeftUserControls.Name = "PanelBottomLeftUserControls";
            this.PanelBottomLeftUserControls.Size = new System.Drawing.Size(229, 116);
            this.PanelBottomLeftUserControls.TabIndex = 22;
            // 
            // lbCurrentArtist
            // 
            this.lbCurrentArtist.AutoEllipsis = true;
            this.lbCurrentArtist.AutoSize = true;
            this.lbCurrentArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentArtist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbCurrentArtist.Location = new System.Drawing.Point(9, 20);
            this.lbCurrentArtist.MaximumSize = new System.Drawing.Size(150, 15);
            this.lbCurrentArtist.MinimumSize = new System.Drawing.Size(150, 15);
            this.lbCurrentArtist.Name = "lbCurrentArtist";
            this.lbCurrentArtist.Size = new System.Drawing.Size(150, 15);
            this.lbCurrentArtist.TabIndex = 29;
            this.lbCurrentArtist.Text = "Not Playing";
            // 
            // pbAvancar
            // 
            this.pbAvancar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAvancar.Image = ((System.Drawing.Image)(resources.GetObject("pbAvancar.Image")));
            this.pbAvancar.Location = new System.Drawing.Point(199, 58);
            this.pbAvancar.Name = "pbAvancar";
            this.pbAvancar.Size = new System.Drawing.Size(24, 24);
            this.pbAvancar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAvancar.TabIndex = 28;
            this.pbAvancar.TabStop = false;
            this.pbAvancar.Click += new System.EventHandler(this.pbAvancar_Click);
            // 
            // pbRetroceder
            // 
            this.pbRetroceder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("pbRetroceder.Image")));
            this.pbRetroceder.Location = new System.Drawing.Point(129, 58);
            this.pbRetroceder.Name = "pbRetroceder";
            this.pbRetroceder.Size = new System.Drawing.Size(24, 24);
            this.pbRetroceder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRetroceder.TabIndex = 27;
            this.pbRetroceder.TabStop = false;
            this.pbRetroceder.Click += new System.EventHandler(this.pbRetroceder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(9, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Current Song :";
            // 
            // lbCurrentSong
            // 
            this.lbCurrentSong.AutoEllipsis = true;
            this.lbCurrentSong.AutoSize = true;
            this.lbCurrentSong.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentSong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbCurrentSong.Location = new System.Drawing.Point(9, 35);
            this.lbCurrentSong.MaximumSize = new System.Drawing.Size(150, 15);
            this.lbCurrentSong.MinimumSize = new System.Drawing.Size(150, 15);
            this.lbCurrentSong.Name = "lbCurrentSong";
            this.lbCurrentSong.Size = new System.Drawing.Size(150, 15);
            this.lbCurrentSong.TabIndex = 25;
            this.lbCurrentSong.Text = "Not Playing";
            this.lbCurrentSong.MouseHover += new System.EventHandler(this.lbCurrentSong_MouseHover);
            // 
            // pbPlayButton
            // 
            this.pbPlayButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbPlayButton.Image = ((System.Drawing.Image)(resources.GetObject("pbPlayButton.Image")));
            this.pbPlayButton.Location = new System.Drawing.Point(161, 55);
            this.pbPlayButton.Name = "pbPlayButton";
            this.pbPlayButton.Size = new System.Drawing.Size(32, 32);
            this.pbPlayButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayButton.TabIndex = 24;
            this.pbPlayButton.TabStop = false;
            this.pbPlayButton.Click += new System.EventHandler(this.pbPlayButton_Click);
            // 
            // pbSpeaker
            // 
            this.pbSpeaker.Image = ((System.Drawing.Image)(resources.GetObject("pbSpeaker.Image")));
            this.pbSpeaker.InitialImage = null;
            this.pbSpeaker.Location = new System.Drawing.Point(10, 60);
            this.pbSpeaker.Name = "pbSpeaker";
            this.pbSpeaker.Size = new System.Drawing.Size(16, 16);
            this.pbSpeaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSpeaker.TabIndex = 20;
            this.pbSpeaker.TabStop = false;
            // 
            // lbVolume
            // 
            this.lbVolume.AutoSize = true;
            this.lbVolume.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbVolume.Location = new System.Drawing.Point(97, 63);
            this.lbVolume.Name = "lbVolume";
            this.lbVolume.Size = new System.Drawing.Size(25, 13);
            this.lbVolume.TabIndex = 14;
            this.lbVolume.Text = "100";
            // 
            // pbFav
            // 
            this.pbFav.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbFav.Image = ((System.Drawing.Image)(resources.GetObject("pbFav.Image")));
            this.pbFav.Location = new System.Drawing.Point(950, 59);
            this.pbFav.Name = "pbFav";
            this.pbFav.Size = new System.Drawing.Size(28, 28);
            this.pbFav.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFav.TabIndex = 19;
            this.pbFav.TabStop = false;
            this.pbFav.Click += new System.EventHandler(this.pbFav_Click);
            // 
            // pbShuffle
            // 
            this.pbShuffle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbShuffle.Image = ((System.Drawing.Image)(resources.GetObject("pbShuffle.Image")));
            this.pbShuffle.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbShuffle.InitialImage")));
            this.pbShuffle.Location = new System.Drawing.Point(869, 59);
            this.pbShuffle.Name = "pbShuffle";
            this.pbShuffle.Size = new System.Drawing.Size(28, 28);
            this.pbShuffle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbShuffle.TabIndex = 18;
            this.pbShuffle.TabStop = false;
            this.pbShuffle.Click += new System.EventHandler(this.pbShuffle_Click);
            // 
            // pbReplay
            // 
            this.pbReplay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbReplay.Image = ((System.Drawing.Image)(resources.GetObject("pbReplay.Image")));
            this.pbReplay.InitialImage = null;
            this.pbReplay.Location = new System.Drawing.Point(912, 60);
            this.pbReplay.Name = "pbReplay";
            this.pbReplay.Size = new System.Drawing.Size(28, 28);
            this.pbReplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbReplay.TabIndex = 17;
            this.pbReplay.TabStop = false;
            this.pbReplay.Click += new System.EventHandler(this.pbReplay_Click);
            // 
            // lbDuration
            // 
            this.lbDuration.AutoSize = true;
            this.lbDuration.BackColor = System.Drawing.Color.Transparent;
            this.lbDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDuration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbDuration.Location = new System.Drawing.Point(769, 64);
            this.lbDuration.Name = "lbDuration";
            this.lbDuration.Size = new System.Drawing.Size(38, 15);
            this.lbDuration.TabIndex = 15;
            this.lbDuration.Text = "00:00";
            // 
            // panelContainerForUserSongs
            // 
            this.panelContainerForUserSongs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelContainerForUserSongs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelContainerForUserSongs.Controls.Add(this.PanelPubAndEditSong);
            this.panelContainerForUserSongs.Controls.Add(this.PanelCenter);
            this.panelContainerForUserSongs.Controls.Add(this.panelLeftBlockUserPlaylistsLeft);
            this.panelContainerForUserSongs.Location = new System.Drawing.Point(0, -2);
            this.panelContainerForUserSongs.Margin = new System.Windows.Forms.Padding(0);
            this.panelContainerForUserSongs.Name = "panelContainerForUserSongs";
            this.panelContainerForUserSongs.Size = new System.Drawing.Size(1018, 540);
            this.panelContainerForUserSongs.TabIndex = 16;
            // 
            // PanelPubAndEditSong
            // 
            this.PanelPubAndEditSong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelPubAndEditSong.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPubAndEditSong.Controls.Add(this.PanelEditSongs);
            this.PanelPubAndEditSong.Location = new System.Drawing.Point(229, 470);
            this.PanelPubAndEditSong.Name = "PanelPubAndEditSong";
            this.PanelPubAndEditSong.Size = new System.Drawing.Size(45895, 67);
            this.PanelPubAndEditSong.TabIndex = 10;
            // 
            // PanelEditSongs
            // 
            this.PanelEditSongs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelEditSongs.Controls.Add(this.tbEditArtist);
            this.PanelEditSongs.Controls.Add(this.label1);
            this.PanelEditSongs.Controls.Add(this.pbAddArtist);
            this.PanelEditSongs.Controls.Add(this.label3);
            this.PanelEditSongs.Controls.Add(this.tbEditAlbum);
            this.PanelEditSongs.Controls.Add(this.btnApplyChanges);
            this.PanelEditSongs.Controls.Add(this.cbEditArtist);
            this.PanelEditSongs.Controls.Add(this.label4);
            this.PanelEditSongs.Controls.Add(this.tbEditSongname);
            this.PanelEditSongs.Location = new System.Drawing.Point(4, 4);
            this.PanelEditSongs.Name = "PanelEditSongs";
            this.PanelEditSongs.Size = new System.Drawing.Size(780, 83);
            this.PanelEditSongs.TabIndex = 29;
            this.PanelEditSongs.Visible = false;
            // 
            // tbEditArtist
            // 
            this.tbEditArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEditArtist.Location = new System.Drawing.Point(45, 36);
            this.tbEditArtist.Name = "tbEditArtist";
            this.tbEditArtist.Size = new System.Drawing.Size(121, 21);
            this.tbEditArtist.TabIndex = 39;
            this.tbEditArtist.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(80, 17);
            this.label1.MaximumSize = new System.Drawing.Size(150, 15);
            this.label1.MinimumSize = new System.Drawing.Size(40, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 35;
            this.label1.Text = "Artist";
            // 
            // pbAddArtist
            // 
            this.pbAddArtist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAddArtist.Image = ((System.Drawing.Image)(resources.GetObject("pbAddArtist.Image")));
            this.pbAddArtist.Location = new System.Drawing.Point(172, 34);
            this.pbAddArtist.Name = "pbAddArtist";
            this.pbAddArtist.Size = new System.Drawing.Size(24, 24);
            this.pbAddArtist.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddArtist.TabIndex = 34;
            this.pbAddArtist.TabStop = false;
            this.pbAddArtist.Click += new System.EventHandler(this.pbAddArtist_Click);
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(281, 17);
            this.label3.MaximumSize = new System.Drawing.Size(150, 15);
            this.label3.MinimumSize = new System.Drawing.Size(40, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 15);
            this.label3.TabIndex = 36;
            this.label3.Text = "Album";
            // 
            // tbEditAlbum
            // 
            this.tbEditAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEditAlbum.Location = new System.Drawing.Point(243, 36);
            this.tbEditAlbum.Name = "tbEditAlbum";
            this.tbEditAlbum.Size = new System.Drawing.Size(127, 21);
            this.tbEditAlbum.TabIndex = 31;
            // 
            // btnApplyChanges
            // 
            this.btnApplyChanges.Location = new System.Drawing.Point(652, 31);
            this.btnApplyChanges.Name = "btnApplyChanges";
            this.btnApplyChanges.Size = new System.Drawing.Size(75, 23);
            this.btnApplyChanges.TabIndex = 33;
            this.btnApplyChanges.Text = "Done";
            this.btnApplyChanges.UseVisualStyleBackColor = true;
            this.btnApplyChanges.Click += new System.EventHandler(this.btnApplyChanges_Click);
            // 
            // cbEditArtist
            // 
            this.cbEditArtist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.cbEditArtist.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbEditArtist.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cbEditArtist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.cbEditArtist.FormattingEnabled = true;
            this.cbEditArtist.IntegralHeight = false;
            this.cbEditArtist.ItemHeight = 19;
            this.cbEditArtist.Location = new System.Drawing.Point(45, 33);
            this.cbEditArtist.MaximumSize = new System.Drawing.Size(150, 0);
            this.cbEditArtist.MaxLength = 15;
            this.cbEditArtist.MinimumSize = new System.Drawing.Size(20, 0);
            this.cbEditArtist.Name = "cbEditArtist";
            this.cbEditArtist.Size = new System.Drawing.Size(121, 25);
            this.cbEditArtist.Sorted = true;
            this.cbEditArtist.Style = MetroFramework.MetroColorStyle.Orange;
            this.cbEditArtist.TabIndex = 38;
            this.cbEditArtist.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cbEditArtist.UseCustomBackColor = true;
            this.cbEditArtist.UseCustomForeColor = true;
            this.cbEditArtist.UseSelectable = true;
            this.cbEditArtist.UseStyleColors = true;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(504, 18);
            this.label4.MaximumSize = new System.Drawing.Size(150, 15);
            this.label4.MinimumSize = new System.Drawing.Size(40, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 15);
            this.label4.TabIndex = 37;
            this.label4.Text = "Name";
            // 
            // tbEditSongname
            // 
            this.tbEditSongname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEditSongname.Location = new System.Drawing.Point(462, 36);
            this.tbEditSongname.Name = "tbEditSongname";
            this.tbEditSongname.Size = new System.Drawing.Size(131, 21);
            this.tbEditSongname.TabIndex = 32;
            // 
            // PanelCenter
            // 
            this.PanelCenter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelCenter.Controls.Add(this.panelFieldIndicators);
            this.PanelCenter.Controls.Add(this.panelSongsContainer);
            this.PanelCenter.Location = new System.Drawing.Point(229, 3);
            this.PanelCenter.Margin = new System.Windows.Forms.Padding(0);
            this.PanelCenter.Name = "PanelCenter";
            this.PanelCenter.Size = new System.Drawing.Size(20427, 558);
            this.PanelCenter.TabIndex = 0;
            this.PanelCenter.MouseLeave += new System.EventHandler(this.PanelCenter_MouseLeave);
            // 
            // panelFieldIndicators
            // 
            this.panelFieldIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFieldIndicators.Controls.Add(this.lblOrderByDuration);
            this.panelFieldIndicators.Controls.Add(this.lblOrderByAlbum);
            this.panelFieldIndicators.Controls.Add(this.lblOrderByArtist);
            this.panelFieldIndicators.Controls.Add(this.lblOrderByName);
            this.panelFieldIndicators.Controls.Add(this.label11);
            this.panelFieldIndicators.Controls.Add(this.lblOrderByEntry);
            this.panelFieldIndicators.Controls.Add(this.pbDefaultStarOrder);
            this.panelFieldIndicators.Location = new System.Drawing.Point(-1, 5);
            this.panelFieldIndicators.Name = "panelFieldIndicators";
            this.panelFieldIndicators.Size = new System.Drawing.Size(787, 27);
            this.panelFieldIndicators.TabIndex = 23;
            // 
            // lblOrderByDuration
            // 
            this.lblOrderByDuration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblOrderByDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderByDuration.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblOrderByDuration.Location = new System.Drawing.Point(693, 3);
            this.lblOrderByDuration.MaximumSize = new System.Drawing.Size(250, 25);
            this.lblOrderByDuration.Name = "lblOrderByDuration";
            this.lblOrderByDuration.Size = new System.Drawing.Size(74, 20);
            this.lblOrderByDuration.TabIndex = 39;
            this.lblOrderByDuration.Tag = "Duration";
            this.lblOrderByDuration.Text = "Duration";
            this.lblOrderByDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderByDuration.Click += new System.EventHandler(this.OrderByField);
            // 
            // lblOrderByAlbum
            // 
            this.lblOrderByAlbum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblOrderByAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderByAlbum.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblOrderByAlbum.Location = new System.Drawing.Point(540, 3);
            this.lblOrderByAlbum.MaximumSize = new System.Drawing.Size(100, 25);
            this.lblOrderByAlbum.Name = "lblOrderByAlbum";
            this.lblOrderByAlbum.Size = new System.Drawing.Size(51, 20);
            this.lblOrderByAlbum.TabIndex = 38;
            this.lblOrderByAlbum.Tag = "Album";
            this.lblOrderByAlbum.Text = "Album";
            this.lblOrderByAlbum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderByAlbum.Click += new System.EventHandler(this.OrderByField);
            // 
            // lblOrderByArtist
            // 
            this.lblOrderByArtist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblOrderByArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderByArtist.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblOrderByArtist.Location = new System.Drawing.Point(346, 2);
            this.lblOrderByArtist.MaximumSize = new System.Drawing.Size(53, 25);
            this.lblOrderByArtist.MinimumSize = new System.Drawing.Size(53, 20);
            this.lblOrderByArtist.Name = "lblOrderByArtist";
            this.lblOrderByArtist.Size = new System.Drawing.Size(53, 20);
            this.lblOrderByArtist.TabIndex = 37;
            this.lblOrderByArtist.Tag = "Artist";
            this.lblOrderByArtist.Text = "Artist";
            this.lblOrderByArtist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderByArtist.Click += new System.EventHandler(this.OrderByField);
            // 
            // lblOrderByName
            // 
            this.lblOrderByName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblOrderByName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderByName.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblOrderByName.Location = new System.Drawing.Point(128, 7);
            this.lblOrderByName.MaximumSize = new System.Drawing.Size(39, 13);
            this.lblOrderByName.Name = "lblOrderByName";
            this.lblOrderByName.Size = new System.Drawing.Size(39, 13);
            this.lblOrderByName.TabIndex = 36;
            this.lblOrderByName.Tag = "Name";
            this.lblOrderByName.Text = "Name";
            this.lblOrderByName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderByName.Click += new System.EventHandler(this.OrderByField);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.BackColor = System.Drawing.Color.CadetBlue;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(3, 25);
            this.label11.MaximumSize = new System.Drawing.Size(787, 1);
            this.label11.MinimumSize = new System.Drawing.Size(787, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(787, 1);
            this.label11.TabIndex = 35;
            this.label11.Text = resources.GetString("label11.Text");
            // 
            // lblOrderByEntry
            // 
            this.lblOrderByEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOrderByEntry.AutoSize = true;
            this.lblOrderByEntry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderByEntry.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblOrderByEntry.Location = new System.Drawing.Point(39, 8);
            this.lblOrderByEntry.MaximumSize = new System.Drawing.Size(15, 13);
            this.lblOrderByEntry.Name = "lblOrderByEntry";
            this.lblOrderByEntry.Size = new System.Drawing.Size(15, 13);
            this.lblOrderByEntry.TabIndex = 34;
            this.lblOrderByEntry.Text = "#";
            // 
            // pbDefaultStarOrder
            // 
            this.pbDefaultStarOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbDefaultStarOrder.Image = ((System.Drawing.Image)(resources.GetObject("pbDefaultStarOrder.Image")));
            this.pbDefaultStarOrder.Location = new System.Drawing.Point(5, 5);
            this.pbDefaultStarOrder.Margin = new System.Windows.Forms.Padding(0);
            this.pbDefaultStarOrder.MaximumSize = new System.Drawing.Size(16, 16);
            this.pbDefaultStarOrder.Name = "pbDefaultStarOrder";
            this.pbDefaultStarOrder.Size = new System.Drawing.Size(16, 16);
            this.pbDefaultStarOrder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDefaultStarOrder.TabIndex = 25;
            this.pbDefaultStarOrder.TabStop = false;
            this.pbDefaultStarOrder.Tag = "Favourite";
            this.pbDefaultStarOrder.Click += new System.EventHandler(this.OrderByField);
            // 
            // panelSongsContainer
            // 
            this.panelSongsContainer.AutoScroll = true;
            this.panelSongsContainer.Location = new System.Drawing.Point(2, 32);
            this.panelSongsContainer.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.panelSongsContainer.Name = "panelSongsContainer";
            this.panelSongsContainer.Size = new System.Drawing.Size(800, 430);
            this.panelSongsContainer.TabIndex = 0;
            this.panelSongsContainer.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSongsContainer_Paint);
            this.panelSongsContainer.MouseEnter += new System.EventHandler(this.panelSongsContainer_MouseEnter);
            // 
            // panelLeftBlockUserPlaylistsLeft
            // 
            this.panelLeftBlockUserPlaylistsLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelLeftBlockUserPlaylistsLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLeftBlockUserPlaylistsLeft.Controls.Add(this.lbAddNewPlaylist);
            this.panelLeftBlockUserPlaylistsLeft.Controls.Add(this.PanelUserPlaylist);
            this.panelLeftBlockUserPlaylistsLeft.Location = new System.Drawing.Point(-1, 3);
            this.panelLeftBlockUserPlaylistsLeft.Margin = new System.Windows.Forms.Padding(0);
            this.panelLeftBlockUserPlaylistsLeft.Name = "panelLeftBlockUserPlaylistsLeft";
            this.panelLeftBlockUserPlaylistsLeft.Size = new System.Drawing.Size(3000, 533);
            this.panelLeftBlockUserPlaylistsLeft.TabIndex = 9;
            // 
            // lbAddNewPlaylist
            // 
            this.lbAddNewPlaylist.BackColor = System.Drawing.Color.Transparent;
            this.lbAddNewPlaylist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbAddNewPlaylist.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lbAddNewPlaylist.ForeColor = System.Drawing.Color.Chocolate;
            this.lbAddNewPlaylist.Location = new System.Drawing.Point(9, 2);
            this.lbAddNewPlaylist.MaximumSize = new System.Drawing.Size(214, 30);
            this.lbAddNewPlaylist.MinimumSize = new System.Drawing.Size(214, 30);
            this.lbAddNewPlaylist.Name = "lbAddNewPlaylist";
            this.lbAddNewPlaylist.Size = new System.Drawing.Size(214, 30);
            this.lbAddNewPlaylist.Style = MetroFramework.MetroColorStyle.Lime;
            this.lbAddNewPlaylist.TabIndex = 0;
            this.lbAddNewPlaylist.Text = "New Playlist";
            this.lbAddNewPlaylist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbAddNewPlaylist.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lbAddNewPlaylist.UseCustomBackColor = true;
            this.lbAddNewPlaylist.UseCustomForeColor = true;
            this.lbAddNewPlaylist.Click += new System.EventHandler(this.lbAddNewPlaylist_Click);
            this.lbAddNewPlaylist.MouseEnter += new System.EventHandler(this.lbAddNewPlaylist_MouseEnter);
            this.lbAddNewPlaylist.MouseLeave += new System.EventHandler(this.lbAddNewPlaylist_MouseLeave);
            // 
            // PanelUserPlaylist
            // 
            this.PanelUserPlaylist.Location = new System.Drawing.Point(9, 32);
            this.PanelUserPlaylist.MaximumSize = new System.Drawing.Size(210, 450);
            this.PanelUserPlaylist.MinimumSize = new System.Drawing.Size(160, 200);
            this.PanelUserPlaylist.Name = "PanelUserPlaylist";
            this.PanelUserPlaylist.Size = new System.Drawing.Size(210, 450);
            this.PanelUserPlaylist.TabIndex = 18;
            // 
            // panelNavigator
            // 
            this.panelNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelNavigator.Controls.Add(this.panel1);
            this.panelNavigator.Controls.Add(this.panelSearch);
            this.panelNavigator.Location = new System.Drawing.Point(7, 47);
            this.panelNavigator.Name = "panelNavigator";
            this.panelNavigator.Size = new System.Drawing.Size(1006, 28);
            this.panelNavigator.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.metroLabel2);
            this.panel1.Controls.Add(this.metroLabel3);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(51, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(103, 27);
            this.panel1.TabIndex = 35;
            this.panel1.Click += new System.EventHandler(this.SongMenu_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.BackColor = System.Drawing.Color.CadetBlue;
            this.metroLabel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.metroLabel2.Enabled = false;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.ForeColor = System.Drawing.Color.Chocolate;
            this.metroLabel2.Location = new System.Drawing.Point(0, 22);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.metroLabel2.Name = "metroRowStandardLabel";
            this.metroLabel2.Size = new System.Drawing.Size(102, 2);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel2.TabIndex = 32;
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroLabel3.Enabled = false;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.ForeColor = System.Drawing.Color.OrangeRed;
            this.metroLabel3.Location = new System.Drawing.Point(0, 1);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.metroLabel3.Name = "metroLabel7";
            this.metroLabel3.Size = new System.Drawing.Size(102, 21);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel3.TabIndex = 28;
            this.metroLabel3.Text = "Songs";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // panelSearch
            // 
            this.panelSearch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelSearch.BackColor = System.Drawing.Color.Transparent;
            this.panelSearch.Controls.Add(this.metroLabel6);
            this.panelSearch.Controls.Add(this.metroLabel7);
            this.panelSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelSearch.Location = new System.Drawing.Point(898, 3);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(103, 27);
            this.panelSearch.TabIndex = 34;
            this.panelSearch.Click += new System.EventHandler(this.panelSearch_Click);
            // 
            // metroLabel6
            // 
            this.metroLabel6.BackColor = System.Drawing.Color.CadetBlue;
            this.metroLabel6.Cursor = System.Windows.Forms.Cursors.Default;
            this.metroLabel6.Enabled = false;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.ForeColor = System.Drawing.Color.Chocolate;
            this.metroLabel6.Location = new System.Drawing.Point(0, 22);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(0);
            this.metroLabel6.Name = "metroRowStandardLabel";
            this.metroLabel6.Size = new System.Drawing.Size(102, 2);
            this.metroLabel6.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel6.TabIndex = 32;
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel6.UseCustomBackColor = true;
            this.metroLabel6.UseCustomForeColor = true;
            this.metroLabel6.Visible = false;
            // 
            // metroLabel7
            // 
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroLabel7.Enabled = false;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.ForeColor = System.Drawing.Color.OrangeRed;
            this.metroLabel7.Location = new System.Drawing.Point(0, 1);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(102, 21);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel7.TabIndex = 28;
            this.metroLabel7.Text = "Search";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel7.UseCustomBackColor = true;
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // PanelMainTop
            // 
            this.PanelMainTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelMainTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelMainTop.Controls.Add(this.pbMinimize);
            this.PanelMainTop.Controls.Add(this.pbMaximize);
            this.PanelMainTop.Controls.Add(this.pbClose);
            this.PanelMainTop.Controls.Add(this.panel5);
            this.PanelMainTop.Controls.Add(this.panelNavigator);
            this.PanelMainTop.Location = new System.Drawing.Point(3, 3);
            this.PanelMainTop.Name = "PanelMainTop";
            this.PanelMainTop.Size = new System.Drawing.Size(1018, 93);
            this.PanelMainTop.TabIndex = 170;
            this.PanelMainTop.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelMainTop_Paint);
            this.PanelMainTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainWindow_MouseDown);
            // 
            // pbMinimize
            // 
            this.pbMinimize.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pbMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbMinimize.Image = ((System.Drawing.Image)(resources.GetObject("pbMinimize.Image")));
            this.pbMinimize.InitialImage = null;
            this.pbMinimize.Location = new System.Drawing.Point(933, -1);
            this.pbMinimize.Name = "pbMinimize";
            this.pbMinimize.Size = new System.Drawing.Size(28, 28);
            this.pbMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMinimize.TabIndex = 30;
            this.pbMinimize.TabStop = false;
            this.pbMinimize.Click += new System.EventHandler(this.pbMinimize_Click);
            // 
            // pbMaximize
            // 
            this.pbMaximize.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pbMaximize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbMaximize.Image = ((System.Drawing.Image)(resources.GetObject("pbMaximize.Image")));
            this.pbMaximize.InitialImage = null;
            this.pbMaximize.Location = new System.Drawing.Point(961, -1);
            this.pbMaximize.Name = "pbMaximize";
            this.pbMaximize.Size = new System.Drawing.Size(28, 28);
            this.pbMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMaximize.TabIndex = 29;
            this.pbMaximize.TabStop = false;
            this.pbMaximize.Click += new System.EventHandler(this.pbMaximize_Click);
            // 
            // pbClose
            // 
            this.pbClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pbClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbClose.Image = ((System.Drawing.Image)(resources.GetObject("pbClose.Image")));
            this.pbClose.InitialImage = null;
            this.pbClose.Location = new System.Drawing.Point(989, -1);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(28, 28);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbClose.TabIndex = 28;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.pbClose_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.lblTitle);
            this.panel5.Location = new System.Drawing.Point(455, 2);
            this.panel5.MaximumSize = new System.Drawing.Size(500, 60);
            this.panel5.MinimumSize = new System.Drawing.Size(290, 29);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(290, 29);
            this.panel5.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(290, 29);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "Tumacia Player";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelMainCenterContainer
            // 
            this.panelMainCenterContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMainCenterContainer.Controls.Add(this.panelContainerForUserSongs);
            this.panelMainCenterContainer.Location = new System.Drawing.Point(3, 83);
            this.panelMainCenterContainer.Margin = new System.Windows.Forms.Padding(0);
            this.panelMainCenterContainer.Name = "panelMainCenterContainer";
            this.panelMainCenterContainer.Size = new System.Drawing.Size(1018, 538);
            this.panelMainCenterContainer.TabIndex = 6;
            // 
            // SongsContextMenu
            // 
            this.SongsContextMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.SongsContextMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongsContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmPlay,
            this.tsmAddTo,
            this.removeToolStripMenuItem,
            this.eraseToolStripMenuItem,
            this.tdmEdit});
            this.SongsContextMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.SongsContextMenu.Name = "SongsContextMenu";
            this.SongsContextMenu.Size = new System.Drawing.Size(121, 114);
            this.SongsContextMenu.Style = MetroFramework.MetroColorStyle.Magenta;
            this.SongsContextMenu.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.SongsContextMenu.UseCustomBackColor = true;
            this.SongsContextMenu.UseCustomForeColor = true;
            this.SongsContextMenu.UseStyleColors = true;
            this.SongsContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.SongsContextMenu_Opening);
            // 
            // tsmPlay
            // 
            this.tsmPlay.Name = "tsmPlay";
            this.tsmPlay.Size = new System.Drawing.Size(120, 22);
            this.tsmPlay.Text = "Play";
            this.tsmPlay.Click += new System.EventHandler(this.tsmPlay_Click);
            // 
            // tsmAddTo
            // 
            this.tsmAddTo.Name = "tsmAddTo";
            this.tsmAddTo.Size = new System.Drawing.Size(120, 22);
            this.tsmAddTo.Text = "Add to :";
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // eraseToolStripMenuItem
            // 
            this.eraseToolStripMenuItem.Name = "eraseToolStripMenuItem";
            this.eraseToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.eraseToolStripMenuItem.Text = "Erase";
            this.eraseToolStripMenuItem.Click += new System.EventHandler(this.eraseToolStripMenuItem_Click);
            // 
            // tdmEdit
            // 
            this.tdmEdit.Name = "tdmEdit";
            this.tdmEdit.Size = new System.Drawing.Size(120, 22);
            this.tdmEdit.Text = "Edit";
            this.tdmEdit.Click += new System.EventHandler(this.tdmEdit_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(540, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Album";
            // 
            // PlaylistContextMenu
            // 
            this.PlaylistContextMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.PlaylistContextMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaylistContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playToolStripMenuItem,
            this.removeToolStripMenuItem1});
            this.PlaylistContextMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.PlaylistContextMenu.Name = "PlaylistContextMenu";
            this.PlaylistContextMenu.Size = new System.Drawing.Size(121, 48);
            this.PlaylistContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.PlaylistContextMenu_Opening);
            // 
            // playToolStripMenuItem
            // 
            this.playToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.playToolStripMenuItem.Name = "playToolStripMenuItem";
            this.playToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.playToolStripMenuItem.Text = "Play";
            this.playToolStripMenuItem.Click += new System.EventHandler(this.playToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem1
            // 
            this.removeToolStripMenuItem1.Name = "removeToolStripMenuItem1";
            this.removeToolStripMenuItem1.Size = new System.Drawing.Size(120, 22);
            this.removeToolStripMenuItem1.Text = "Remove";
            this.removeToolStripMenuItem1.Click += new System.EventHandler(this.removePlaylistToolStripMenuItem_Click);
            // 
            // MainWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(1024, 720);
            this.Controls.Add(this.PanelMainBottom);
            this.Controls.Add(this.panelMainCenterContainer);
            this.Controls.Add(this.PanelMainTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 400);
            this.Name = "MainWindowForm";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tumacia Player";
            this.Load += new System.EventHandler(this.MainWindowForm_Load);
            this.ClientSizeChanged += new System.EventHandler(this.MainWindowForm_ClientSizeChanged);
            this.SizeChanged += new System.EventHandler(this.MainWindowForm_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainWindow_MouseDown);
            this.Resize += new System.EventHandler(this.MainWindowForm_Resize);
            this.PanelMainBottom.ResumeLayout(false);
            this.PanelMainBottom.PerformLayout();
            this.PanelBottomLeftUserControls.ResumeLayout(false);
            this.PanelBottomLeftUserControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAvancar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRetroceder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpeaker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFav)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbShuffle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReplay)).EndInit();
            this.panelContainerForUserSongs.ResumeLayout(false);
            this.PanelPubAndEditSong.ResumeLayout(false);
            this.PanelEditSongs.ResumeLayout(false);
            this.PanelEditSongs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddArtist)).EndInit();
            this.PanelCenter.ResumeLayout(false);
            this.panelFieldIndicators.ResumeLayout(false);
            this.panelFieldIndicators.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDefaultStarOrder)).EndInit();
            this.panelLeftBlockUserPlaylistsLeft.ResumeLayout(false);
            this.panelNavigator.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelSearch.ResumeLayout(false);
            this.PanelMainTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panelMainCenterContainer.ResumeLayout(false);
            this.SongsContextMenu.ResumeLayout(false);
            this.PlaylistContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }




        #endregion

        private Panel PanelMainBottom;
        private Label lbVolume;
        private Label lbDuration;
        private Panel panelContainerForUserSongs;
        private Panel panelLeftBlockUserPlaylistsLeft;
        private Panel PanelCenter;
        private Panel PanelMainTop;
        private Panel PanelUserPlaylist;
        private PictureBox pbFav;
        private PictureBox pbShuffle;
        private PictureBox pbReplay;
        private PictureBox pbSpeaker;
        private Panel PanelBottomLeftUserControls;
        private Panel panelSongPosition;
        private PictureBox pbPlayButton;
        private Label lbCurrentTime;
        private Label label2;
        private Label lbCurrentSong;
        private MetroFramework.Controls.MetroLabel lbAddNewPlaylist;
        private Panel panelMainCenterContainer;
        private Panel panelFieldIndicators;
        private MetroFramework.Controls.MetroContextMenu SongsContextMenu;
        private ToolStripMenuItem tsmPlay;
        private ToolStripMenuItem tsmAddTo;
        private PictureBox pbAvancar;
        private PictureBox pbRetroceder;
        private ToolStripMenuItem removeToolStripMenuItem;
        private ToolStripMenuItem eraseToolStripMenuItem;
        private PictureBox pbDefaultStarOrder;
        private Label lbCurrentArtist;
        private Panel panelNavigator;
        private Panel panelSearch;
        private ToolStripMenuItem tdmEdit;
        private Panel PanelPubAndEditSong;
        private Panel panel1;
        private Panel panelSongsContainer;
        private Label label6;
        private Label lblOrderByEntry;
        private Label label11;
        private MetroFramework.Controls.MetroContextMenu PlaylistContextMenu;
        private ToolStripMenuItem playToolStripMenuItem;
        private ToolStripMenuItem removeToolStripMenuItem1;
        private Panel PanelEditSongs;
        private TextBox tbEditArtist;
        private Label label1;
        private PictureBox pbAddArtist;
        private Label label3;
        private TextBox tbEditAlbum;
        private Button btnApplyChanges;
        private MetroFramework.Controls.MetroComboBox cbEditArtist;
        private Label label4;
        private TextBox tbEditSongname;
        private Label lblTitle;
        private Panel panel5;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private Label lblOrderByName;
        private Label lblOrderByDuration;
        private Label lblOrderByAlbum;
        private Label lblOrderByArtist;
        private PictureBox pbMinimize;
        private PictureBox pbMaximize;
        private PictureBox pbClose;
    }
}

