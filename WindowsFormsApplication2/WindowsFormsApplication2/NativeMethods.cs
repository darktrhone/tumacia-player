﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public static class NativeMethods
    {
        public const int WmNclbuttondown = 0xA1;
        public const int HtCaption = 0x2;

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        private static extern bool ReleaseCapture();
        
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);





        public class GlobalHotkey
        {

            private readonly int _modifier;
            private readonly int _key;
            private readonly IntPtr _hWnd;
            private readonly int _id;

            public GlobalHotkey(int modifier, Keys key, Form form)
            {
                _modifier = modifier;
                _key = (int)key;
                _hWnd = form.Handle;
                _id = GetHashCode();

            }
            public override sealed int GetHashCode()
            {
                return _modifier ^ _key ^ _hWnd.ToInt32();
            }
            public bool Register()
            {
                return RegisterHotKey(_hWnd, _id, _modifier, _key);
            }

            public bool Unregiser()
            {
                return UnregisterHotKey(_hWnd, _id);
            }


        }


        public static void PSendMessage(IntPtr handler)
        {
            SendMessage(handler, WmNclbuttondown, HtCaption, 0);
        }

        public static void PReleaseCapture()
        {
            ReleaseCapture();
        }




    }
}
