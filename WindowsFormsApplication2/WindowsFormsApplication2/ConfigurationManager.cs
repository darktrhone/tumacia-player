﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;
using System.Windows.Forms;
using Microsoft.Win32;

namespace WindowsFormsApplication2
{
    internal static class ConfigurationManager
    {


        public static void SetBrowserEmulation()
        {
             
            RegistryKey rk9 = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Internet Explorer\\BrowserEmulation", RegistryKeyPermissionCheck.ReadWriteSubTree);
            if (rk9 != null) rk9.SetValue("Tumacia Player.exe", 10001, RegistryValueKind.DWord);


            RegistryKey rk8 =Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Internet Explorer\\BrowserEmulation",RegistryKeyPermissionCheck.ReadWriteSubTree);
            if (rk8 != null) rk8.SetValue("Tumacia Player.exe", 10001, RegistryValueKind.DWord);


            RegistryKey rk4 = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree);
            if (rk4 != null) rk4.SetValue("Tumacia Player.exe", 10001, RegistryValueKind.DWord);

            RegistryKey rk5 = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree);
            if (rk5 != null) rk5.SetValue("Tumacia Player.exe", 10001, RegistryValueKind.DWord);
        }

        public static void RegisterHotkeys(MainWindowForm window)
        {
            try
            {
               new NativeMethods.GlobalHotkey(0, Keys.MediaNextTrack, window).Register();
               new NativeMethods.GlobalHotkey(0, Keys.MediaPreviousTrack, window).Register();
               new NativeMethods.GlobalHotkey(0, Keys.MediaPlayPause, window).Register();
            }
            catch 
            {
                //ignore
            }
        }

        internal static bool RunAsAdmin()
        {
            var wi = WindowsIdentity.GetCurrent();
            if (wi != null)
            {
                var wp = new WindowsPrincipal(wi);

                var isAdmin = wp.IsInRole(WindowsBuiltInRole.Administrator);
                if (isAdmin) return true;
            }


            var processInfo = new ProcessStartInfo(Assembly.GetExecutingAssembly().CodeBase)
            {
                UseShellExecute = true,
                Verb = "runas"
            };
            try
            {
                Process.Start(processInfo);
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, this application must be run as Administrator.");
            }

            return false;
        }
    }
}
