﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{

    internal static class TmcPlayer
    {
        public static List<Playlist> Library;

        public static Timer SongPositionTimer;
        public static string MusicFolder { get; set; }
        public static string DownloadFolder { get; set; }
        public static string DataFolder { get; set; }



        static TmcPlayer()
        {
            MusicFolder = FileHandler.CreateMusicFolder();
            DownloadFolder = FileHandler.CreateDownloadsFolder();
            DataFolder = FileHandler.CreateDataFolder();
            Library = FileHandler.PopulateLibraryFromXml(Path.Combine(DataFolder, "Playlists.xml"));
        }



        public static void EditSong(string pl, string songname, string newSongname, string artist, string album)
        {
            try
            {
                newSongname = Path.GetInvalidFileNameChars()
                    .Aggregate(newSongname, (current, c) => current.Replace(c.ToString(), string.Empty));

                Library.ForEach(p =>
                {
                    Song s = FindSong(p.Name, songname);
                    if (s != null)
                    {
                        s.Name = newSongname;
                        s.Artist = artist;
                        s.Album = album;
                    }
                });

                File.Move(Path.Combine(MusicFolder, songname + ".mp3"), Path.Combine(MusicFolder, newSongname + ".mp3"));
                FileHandler.SavePlaylist(Library, DataFolder);
            }
            catch
            {
                MessageBox.Show("Não pode editar uma música que está a tocar!");
            }

        }

        public static void EraseSongFromLibrary(string song)
        {
            foreach (var index in Library.Select(i => Library.IndexOf(i)))
            {
                Library[index].PlayListSongs.RemoveAll(x => x.Name == song);
                File.Delete(Path.Combine(MusicFolder, song + ".mp3"));
            }
        }

        public static bool AddNewPlayList(Playlist plToAdd)
        {
            if (FindPlaylist(plToAdd.Name) != null) return false;

            Library.Add(plToAdd);
            return true;
        }

        public static bool AddNewPlayList(string plToAdd)
        {
            if (FindPlaylist(plToAdd) != null) return false;

            Library.Add(new Playlist(plToAdd) {PlayListSongs = new List<Song>()});
            return true;
        }

        public static void RemovePlayList(string playlistName)
        {
            if (UserPreferences.SelectedPlaylist == playlistName)
                UserPreferences.SelectedPlaylist = "All Songs";

            Library.Remove(FindPlaylist(playlistName));
        }

        public static void RemovePlayList(Playlist playlist)
        {
            Library.Remove(FindPlaylist(playlist.Name));
        }

        public static Playlist FindPlaylist(string name)
        {
            return Library.Find(p => p.Name == name);
        }

        public static void AddNewSongToPlaylist(string playlist, string songName)
        {
            if (FindSong(playlist, songName) != null)
                return;


            foreach (Song s in Library.Select(p => FindSong(p.Name, songName)).Where(s => s != null))
            {
                Library[GetPlaylistIndex(playlist)].PlayListSongs.Add(s);
                return;
            }


        }

        public static bool AddNewSongToPlaylist(string playlist, Song s)
        {

            if (FindSong(playlist, s.Name) != null)
            {
                MessageBox.Show("musica já existe na playlist");
                return false;
            }

            Library[GetPlaylistIndex(playlist)].PlayListSongs.Add(s);
            return true;

        }



        public static Song FindSong(string playlist, string songName)
        {
            return Library[GetPlaylistIndex(playlist)].PlayListSongs.Find(x => x.Name == songName);
        }

        public static int FindSongIndex(string playlist, string songName)
        {
            return Library[GetPlaylistIndex(playlist)].PlayListSongs.FindIndex(x => x.Name == songName);
        }

        public static bool SongExists(string playlist, string songName)
        {
            int index = GetPlaylistIndex(playlist);
            return index != -1 && Library[index].PlayListSongs.Exists(x => x.Name == songName);
        }

        public static void RemoveSong(string playlist, string song)
        {
            int index = Library.FindIndex(x => x.Name == playlist);
            Library[index].PlayListSongs.RemoveAll(x => x.Name == song);
        }

        public static void SetSongAsFavourite(string song, string playlist)
        {
            Library[GetPlaylistIndex(playlist)].PlayListSongs[GetSongIndex(playlist, song)].IsFavourite ^= 1;
        }

        public static bool IsSongFavourite(string playlist, string song)
        {
            try
            {
                return Library[GetPlaylistIndex(playlist)].PlayListSongs[GetSongIndex(playlist, song)].IsFavourite == 1;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string PlayNewPlaylist(string playlist)
        {
            Playlist p = FindPlaylist(playlist);
            if (p.PlayListSongs.Count == 0)
                return String.Empty;
            
            UserPreferences.SelectedPlaylist = playlist;


            UserPreferences.SongsPlayed.Clear();
            UserPreferences.SongsToReplay.Clear();
            UserPreferences.SongsToPlayShuffle = new List<Song>(p.PlayListSongs);
            
            if (!UserPreferences.IsShuffle)
            {
                UserPreferences.SelectedSong = p.PlayListSongs[0].Name;
                return p.PlayListSongs[0].Name;
            }


            int index = new Random().Next(0, UserPreferences.SongsToPlayShuffle.Count - 1);
                  
            UserPreferences.SelectedSong = UserPreferences.SongsToPlayShuffle[index].Name;
            UserPreferences.RemoveFromShuffleSongs(index);

            return UserPreferences.SelectedSong;
        }
        public static string GetNextSong(string playlist = null, string currentSong = null)
        {
            if (playlist == null)
                playlist = MusicPlayer.CurrentPlaylistName;

            if (currentSong == null)
                currentSong = MusicPlayer.CurrentSongName;

            Playlist p = FindPlaylist(playlist);

            if (p.PlayListSongs.Count == 1)
                return String.Empty;


            if (UserPreferences.IsShuffle)
            {

                if (UserPreferences.SongsToPlayShuffle.Count == 0)
                    UserPreferences.SongsToPlayShuffle = new List<Song>(Library[GetPlaylistIndex(MusicPlayer.CurrentPlaylistName)].PlayListSongs);


                string song;
                if (UserPreferences.SongsToReplay.Count > 0) // songs to replay
                {
                    song = UserPreferences.SongsToReplay.Last().Name;
                    UserPreferences.RemoveFirstFromReplayedSongs();
                    return song;
                }
                else // Normal procedure on shuffle
                {
                    int index = new Random().Next(0, UserPreferences.SongsToPlayShuffle.Count - 1);

                    if (index == -1)
                        return string.Empty;


                    song = UserPreferences.SongsToPlayShuffle[index].Name;
                    UserPreferences.RemoveFromShuffleSongs(index);
                    return song;
                }




            }
            else // not shuffle
            {
                //if (UserPreferences.SongsToReplay.Count == 0)
               // {
                
                int index = p.PlayListSongs.FindIndex(x => x.Name == currentSong)+1;

                if (index == p.PlayListSongs.Count)
                    index = 0;

                return p.PlayListSongs[index].Name;
        
                //}
              /*  else // tocar novamente sequencialmente
                {
                    song = UserPreferences.SongsToReplay.Last().Name;
                    UserPreferences.RemoveFirstFromReplayedSongs();
                }*/
            }
        }

        private static int GetPlaylistIndex(string playlist)
        {
            return Library.FindIndex(p => p.Name == playlist);
        }

        private static int GetSongIndex(string playlist, string song)
        {
            try
            {
                return Library[GetPlaylistIndex(playlist)].PlayListSongs.FindIndex(s => s.Name == song);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        internal static string GetSongArtist(string playlist, string currentSongName)
        {
            try
            {
                int idxP = GetPlaylistIndex(playlist);
                int idxS = GetSongIndex(playlist, currentSongName);

                if (idxP >= 0 && idxS >= 0)
                    return Library[idxP].PlayListSongs[idxS].Artist;

                foreach (var s in Library.Select(p => FindSong(p.Name, currentSongName)).Where(s => !string.IsNullOrEmpty(s.Artist)))
                    return s.Artist;
            }
            catch (Exception)
            {
                return "On Preview";
            }
            return "On Preview";
        }

        internal static string GetSongLength(string playlist, string currentSongName)
        {
            try
            {
                int idxP = GetPlaylistIndex(playlist);
                int idxS = GetSongIndex(playlist, currentSongName);

                if (idxP >= 0 && idxS >= 0)
                    return Library[idxP].PlayListSongs[idxS].Duration;

                foreach (var s in Library.Select(p => FindSong(p.Name, currentSongName)).Where(s => !string.IsNullOrEmpty(s.Duration)))                 
                    return s.Duration;
                  
                
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return string.Empty;
        }
    }



    internal class Playlist
        {
            public List<Song> PlayListSongs { get; set; }
            public string Name { get; set; }

            public Playlist(string name)
            {
                Name = name;
                PlayListSongs = new List<Song>();
            }
        }

        internal class Song
        {

            public Song(string name)
            {
                Name = name;
                Album = string.Empty;
                Artist = string.Empty;
                Duration = string.Empty;
                IsFavourite = 0;
            }

            public Song(string name, string duration)
            {
                Name = name;
                Album = string.Empty;
                Artist = string.Empty;
                Duration = duration;
                IsFavourite = 0;
            }

            public Song(string name, string album, string artist, string duration, string isFavourite)
            {
                Name = name;
                Album = album;
                Artist = artist;
                Duration = duration;
                IsFavourite = int.Parse(isFavourite);
            }

            public string Name { get; set; }
            public string Album { get; set; }
            public string Artist { get; set; }
            public string Duration { get; set; }
            public int IsFavourite { get; set; }
        }

    }

