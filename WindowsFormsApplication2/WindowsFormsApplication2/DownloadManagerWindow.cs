﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace WindowsFormsApplication2
{
    public partial class DownloadManagerWindow : UserControl
    {


        public DownloadManagerWindow()
        {
            InitializeComponent();
        }



        private void DownloadManagerWindow_Load(object sender, EventArgs e)
        {
            cbPlaylists.Items.Clear();
            UserPreferences.ListOfPlaylist.ForEach(x=>cbPlaylists.Items.Add(x));
            cbPlaylists.Items.Remove("All Songs");


            cbArtist.Items.Clear();
            UserPreferences.ListOfArtists.ForEach(x => cbArtist.Items.Add(x));
        }

    

        private void btnAddSong_Click(object sender, EventArgs e)
        {
            
            if (lvDownloadDetails.SelectedItems.Count == 0)
            {
                MessageBox.Show("Seleciona uma musica");
                return;
            }

            if (!lvDownloadDetails.SelectedItems[0].SubItems[1].Text.Contains("100"))
            {
                MessageBox.Show("Musica ainda não tá 100%");
                return;
            }
            string playlist = cbPlaylists.Text;

            if (string.IsNullOrEmpty(playlist))
                return;

            string selectedSong = lvDownloadDetails.SelectedItems[0].Text;
          
            string duration = FileHandler.GetSongDuration(selectedSong+".mp3",TmcPlayer.DownloadFolder);

            string artist = cbArtist.Visible ? cbArtist.Text : tbArtist.Text;
            Song s = new Song(tbSongname.Text, tbAlbum.Text, artist, duration, "0");


            // TODO validar nome diferente e pedir reescrição ou modificar

            if (TmcPlayer.AddNewSongToPlaylist(playlist, s))
            {
                string source = Path.Combine(TmcPlayer.DownloadFolder, selectedSong + ".mp3");
                string destination = Path.Combine(TmcPlayer.MusicFolder, tbSongname.Text + ".mp3");

                if (File.Exists(destination))
                    File.Delete(destination);

                File.Move(source, destination);
                lvDownloadDetails.SelectedItems[0].Remove();
            }

     
            tbSongname.Text = "";
            tbAlbum.Text = "";

            FileHandler.SavePlaylist(TmcPlayer.Library, TmcPlayer.DataFolder);
        }



        private void lvDownloadDetails_Click(object sender, EventArgs e)
        {
            if (lvDownloadDetails.SelectedItems.Count <= 0) return;

            string filename = lvDownloadDetails.SelectedItems[0].Text;

            try
            {
                tbSongname.Text = filename.Substring(filename.IndexOf('-') + 1).Trim();

                string artist = filename.Substring(0, filename.IndexOf('-')).Trim();

                if (cbArtist.Items.IndexOf(artist) < 0)
                {
                    UserPreferences.ListOfArtists.Add(artist);
                    cbArtist.Items.Add(artist);
                    UserPreferences.ListOfArtists.Sort();
                }
                cbArtist.SelectedItem = cbArtist.Items[cbArtist.Items.IndexOf(artist)];

            }

            
            catch(Exception)
            {
            }
            
  
        }

        private void btDownload_Click(object sender, EventArgs e)
        {
            string text = txtbxSearch.Text;
            txtbxSearch.Text = "";
            wb.Url = new Uri("https://www.youtube2mp3.cc/");
            StartAsyncDownload(text);

        }

        #region ASYNC

        private async void StartAsyncDownload(string value)
        {
            var retryCount = 0;

            if (String.IsNullOrEmpty(value) || wb.Document == null)
                return;
            
            while (retryCount++ < 10)
            {
                await Task.Delay(500);

                try
                {

                    wb.Document.GetElementById("video").InnerText= value;
                    wb.Document.GetElementById("button").InvokeMember("click");

                    lblStatus.Text = wb.Document.GetElementById("progress_text").InnerText;
                    string url = wb.Document.GetElementById("download").GetAttribute("href");
                    
                    

                    if (String.IsNullOrEmpty(url) || url == "https://www.youtube2mp3.cc/")
                        continue;
                    


                    if (AsyncDownloadManager.FileIsInQueueOrProcessed(url))
                    {
                        MetroMessageBox.Show(ParentForm, "Este ficheiro já está no seu Gestor de Downloads.", "Aviso",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }


                    string filename = AsyncDownloadManager.AddNewUrlToProcess(url, GetFileNameByWB());

                    if (String.IsNullOrEmpty(filename))
                    {
                        //MetroMessageBox.Show(this.ParentForm, "Epa esta porra não sacou nada!", "Erro", MessageBoxButtons.OK,
                        //    MessageBoxIcon.Error);
                        continue;
                    }


                    AddNewDownloadListItem(filename);

                    if (!AsyncDownloadManager.IsBusy)
                        await AsyncDownloadManager.ProcessAllDownloads();

                    lblStatus.Text = "A Processar o download.";
                    wb.Url = new Uri("https://www.youtube2mp3.cc/");
                    return; // correu tudo bem, salta fora

                }
                catch (Exception e)
                {
                    // MetroMessageBox.Show(this, "Aconteceu um erro ao processar o download. Tente novamente.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            if (retryCount >= 30)
            {
                MetroMessageBox.Show(this.ParentForm,
                   "Não foi possível efectuar o download desse ficheiro, tente novamente mais tarde.", "Informação",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private string GetFileNameByWB()
        {
            string filename = wb.Document.GetElementById("title").InnerText;

           // return !String.IsNullOrEmpty(filename) ? filename.Substring(6).Trim() : String.Empty;
            return !String.IsNullOrEmpty(filename) ? filename.Trim() : String.Empty;
        }

        private void AddNewDownloadListItem(string fileName)
        {
            ListViewItem lvi = new ListViewItem(Text = fileName);

            lvi.SubItems.Add(new ListViewItem.ListViewSubItem { Text = "0%" });
            lvDownloadDetails.Items.Add(lvi);


            Timer t = new Timer { Interval = 250, Tag = fileName };

            t.Tick += (sender, args) => t_Tick(sender, fileName);

            t.Start();

        }

        private void t_Tick(object sender, string filename)
        {
            try
            {
                if (String.IsNullOrEmpty(filename))
                    return;

                if (!new FileInfo(Path.Combine(TmcPlayer.DownloadFolder, filename + ".mp3")).Exists)
                    return;

                int count = 0;
                 
                 foreach (var i in from ListViewItem i in lvDownloadDetails.Items where i.Text == filename select i)
                {
                    count = lvDownloadDetails.Items.IndexOf(i);
                }

                SetFileCompletionPercentage(filename, count);

                if (lvDownloadDetails.Items[count].SubItems[1].Text != "100%")
                    return;

                ((Timer)sender).Dispose();

            }
            catch (Exception c)
            {
                MessageBox.Show("errllsls");
                throw;
            }
        }

        private void SetFileCompletionPercentage(string filename, int count)
        {
            FileObject fo = AsyncDownloadManager.FilesColletion.FirstOrDefault(p => p.FileName == filename);

            if (fo == null)
                return;

            try
            {
                string filepath = Path.Combine(TmcPlayer.DownloadFolder, filename + ".mp3");
                long currentLength = new FileInfo(filepath).Length;

                decimal Percentage = Decimal.Divide(currentLength, fo.Length);
                Percentage = Decimal.Round(Decimal.Multiply(Percentage, 100), 2);

                lvDownloadDetails.Items[count].SubItems[1].Text = Percentage + "%";
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private void lvDownloadDetails_DoubleClick(object sender, EventArgs e)
        {
            if (((ListView)sender).SelectedItems.Count == 0)
                return;

            string songName = ((ListView)sender).SelectedItems[0].Text;

            PlaySong(songName);
        }


        #endregion



        private void PlaySong(string songName)
        {
            if (!string.IsNullOrEmpty(songName)) // abrir musica nova
                MusicPlayer.Open(TmcPlayer.DownloadFolder, songName, string.Empty);
            
        }

        private void cbPlaylists_Click(object sender, EventArgs e)
        {
            cbPlaylists.Items.Clear();
            TmcPlayer.Library.ForEach(x => cbPlaylists.Items.Add(x.Name));
            cbPlaylists.Items.Remove("All Songs");

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                var aux = tbSongname.Text;
                tbSongname.Text = cbArtist.Text;
                cbArtist.Text = aux;
            }
            catch (Exception)
            {
            }
        }

        private void pbNewArtist_Click(object sender, EventArgs e)
        {

            if (cbArtist.Visible)
            {
                cbArtist.Hide();
                tbArtist.Show();
            }
            else
            {
                cbArtist.Show();
                tbArtist.Hide();
            }
        }

        private void tbAlbum_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
