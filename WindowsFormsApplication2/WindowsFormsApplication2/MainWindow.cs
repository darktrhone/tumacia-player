﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication2.Properties;
using MetroFramework;
using MetroFramework.Controls;
using Timer = System.Windows.Forms.Timer;

namespace WindowsFormsApplication2
{
    public partial class MainWindowForm : Form
    {
        private ColorSlider _songPositionSlider;
        private ColorSlider _songVolumeSlider;

        #region MAIN WINDOW




        public MainWindowForm()
        {


            if (!ConfigurationManager.RunAsAdmin())
                Close();

            InitializeComponent();





            ConfigurationManager.SetBrowserEmulation();
            ConfigurationManager.RegisterHotkeys(this);

            CreateSliderVolume();
            CreateSliderPosition();

            TmcPlayer.RemovePlayList("All Songs");
            TmcPlayer.AddNewPlayList(FileHandler.PopulateWithAllSongs("All Songs", TmcPlayer.MusicFolder));

            PopulatePlaylistPanel();
            PopulateSongsContextMenuWithPlaylists();

            AsyncDownloadManager.DownloadFolder = TmcPlayer.DownloadFolder;
            cbEditArtist.DataSource = new BindingList<string>(UserPreferences.ListOfArtists);





        }

        protected override void WndProc(ref Message m)
        {

            Keys k = (Keys)(m.LParam.ToInt32() >> 16);
            if (m.Msg == 0x0312)
            {
                switch (k)
                {
                    case Keys.MediaPlayPause:
                        PlaySong();
                        return;

                    case Keys.MediaNextTrack:
                        GetNextSong();
                        return;

                    case Keys.MediaPreviousTrack:
                        GetPreviousSong();
                        return;
                }
            }


           
                bool handled = false;
                const UInt32 WM_NCHITTEST = 0x0084;
                const UInt32 WM_MOUSEMOVE = 0x0200;
                if (m.Msg == WM_NCHITTEST || m.Msg == WM_MOUSEMOVE)
                {

                    const UInt32 HTLEFT = 10;
                    const UInt32 HTRIGHT = 11;
                    const UInt32 HTBOTTOMRIGHT = 17;
                    const UInt32 HTBOTTOM = 15;
                    const UInt32 HTBOTTOMLEFT = 16;
                    const UInt32 HTTOP = 12;
                    const UInt32 HTTOPLEFT = 13;
                    const UInt32 HTTOPRIGHT = 14;
                    const int RESIZE_HANDLE_SIZE = 10;

                    Size formSize = this.Size;
                    Point screenPoint = new Point(m.LParam.ToInt32());
                    Point clientPoint = this.PointToClient(screenPoint);

                    Dictionary<UInt32, Rectangle> boxes = new Dictionary<UInt32, Rectangle>()
                    {
                        {
                            HTBOTTOMLEFT,
                            new Rectangle(0, formSize.Height - RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE,
                                RESIZE_HANDLE_SIZE)
                        },
                        {
                            HTBOTTOM,
                            new Rectangle(RESIZE_HANDLE_SIZE, formSize.Height - RESIZE_HANDLE_SIZE,
                                formSize.Width - 2*RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)
                        },
                        {
                            HTBOTTOMRIGHT,
                            new Rectangle(formSize.Width - RESIZE_HANDLE_SIZE, formSize.Height - RESIZE_HANDLE_SIZE,
                                RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)
                        },
                        {
                            HTRIGHT,
                            new Rectangle(formSize.Width - RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE,
                                formSize.Height - 2*RESIZE_HANDLE_SIZE)
                        },
                        {
                            HTTOPRIGHT,
                            new Rectangle(formSize.Width - RESIZE_HANDLE_SIZE, 0, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)
                        },
                        {
                            HTTOP,
                            new Rectangle(RESIZE_HANDLE_SIZE, 0, formSize.Width - 2*RESIZE_HANDLE_SIZE,
                                RESIZE_HANDLE_SIZE)
                        },
                        {HTTOPLEFT, new Rectangle(0, 0, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)},
                        {
                            HTLEFT,
                            new Rectangle(0, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE,
                                formSize.Height - 2*RESIZE_HANDLE_SIZE)
                        }
                    };

                    foreach (
                        KeyValuePair<uint, Rectangle> hitBox in
                            boxes.Where(hitBox => hitBox.Value.Contains(clientPoint)))
                    {
                        m.Result = (IntPtr) hitBox.Key;
                        handled = true;
                        break;
                    }

                }

           
                if (!handled)
                    base.WndProc(ref m);
            }
      


        



        private void MainWindowForm_Load(object sender, EventArgs e)
        {

            DownloadManagerWindow dmWindow = new DownloadManagerWindow();
            dmWindow.Show();
            panelMainCenterContainer.Controls.Add(dmWindow);

            TmcPlayer.SongPositionTimer = new Timer { Interval = 1000 };
            TmcPlayer.SongPositionTimer.Tick += _SongTimer_Tick;
            TmcPlayer.SongPositionTimer.Start();

        }

        private void MainWindow_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            // handle window drag
            NativeMethods.PReleaseCapture();
            NativeMethods.PSendMessage(Handle);
        }

        private void pbClose_Click(object sender, EventArgs e)
        {
            FileHandler.SavePlaylist(TmcPlayer.Library, TmcPlayer.DataFolder);
            Application.Exit();
        }

        private void pbMaximize_Click(object sender, EventArgs e)
        {
            WindowState = WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;

        }

        private void pbMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        #endregion




















        #region PLAYLIST PANEL


        private Label CreatePlaylistLabel(string name, int position = -1)
        {
            if (position == -1)
                position = TmcPlayer.Library.Count - 1;

            Label lb = new Label
            {
                Text = name,
                Location = new Point(1, 1 + 19 * position),
                BackColor = Color.Tomato,
                BorderStyle = BorderStyle.None,
                Font = new Font("Microsoft Sans Serif", 10F),
                Size = new Size(213, 18),
                Cursor = Cursors.Hand,
                ContextMenuStrip = PlaylistContextMenu,
                Tag = name
            };

            lb.Click += (s, e) =>
            {
                foreach (var a in from object a in PanelUserPlaylist.Controls where a.GetType() == typeof(Label) select a)
                    ((Label)a).BackColor = Color.Tomato;
                lb.BackColor = Color.Red;

                PanelEditSongs.Hide();
                PopulateSongsPanelByPlaylist(lb.Text);


                // certo
                UserPreferences.SelectedPlaylist = lb.Text;
                UserPreferences.SelectedSong = String.Empty;
                UserPreferences.SongsToPlayShuffle = new List<Song>(TmcPlayer.FindPlaylist(lb.Text).PlayListSongs);
                panelSongsContainer.Focus();
            };
            lb.DoubleClick += playToolStripMenuItem_Click;



            return lb;
        }

        private void CreateTextBoxNewPlaylist()
        {
            TextBox tb = ControlBuilder.CreateTextBoxNewPlaylist();

            tb.KeyPress += (s, e) =>
            {
                if ((Keys)e.KeyChar != Keys.Enter)
                    return;

                e.Handled = true;

                if (TmcPlayer.AddNewPlayList(tb.Text))
                {
                    PanelUserPlaylist.Controls.Remove(tb);
                    PanelUserPlaylist.Controls.Add(CreatePlaylistLabel(tb.Text));
                    FileHandler.SavePlaylist(TmcPlayer.Library, TmcPlayer.DataFolder);
                }
            };
            tb.LostFocus += (s, e) =>
            {
                PanelUserPlaylist.Controls.Remove(tb);
            };

            PanelUserPlaylist.Controls.Add(tb);
            tb.Focus();
        }

        private void PopulatePlaylistPanel()
        {
            PanelUserPlaylist.Controls.Clear();
            TmcPlayer.Library.ForEach(p =>
            {
                PanelUserPlaylist.Controls.Add(CreatePlaylistLabel(p.Name, TmcPlayer.Library.IndexOf(p)));
            });
        }

        private void lbAddNewPlaylist_Click(object sender, EventArgs e)
        {
            CreateTextBoxNewPlaylist();
        }

        #endregion


        #region SONGS PANEL CENTER


        private void PopulateSongsPanelByPlaylist(string pl = null, Playlist plObject = null)
        {
            panelSongsContainer.Controls.Clear();
            int index = 0;
            Playlist playList = plObject ?? TmcPlayer.FindPlaylist(pl);
            int offset = 0;
            playList.PlayListSongs.ToList().ForEach(song =>
            {
                if (index > 15)
                    offset = 17;

                    Panel p =  CreatePanelForSongs(index * 31, song.Name);

                p.Controls.Add(ControlBuilder.CreatePbImageAddToFavourite(song));                                    // pbFav

                p.Controls.Add(ControlBuilder.CreateIndexLabelInsidePanelForSongs((++index).ToString()));   // index

                p.Controls.Add(ControlBuilder.CreateNamePanelForSongs(song.Name,offset));             // name

                p.Controls.Add(ControlBuilder.CreateArtistPanelForSongs(song.Artist,offset));          // artist

                p.Controls.Add(ControlBuilder.CreateAlbumPanelForSongs( song.Album,offset));           // album

                p.Controls.Add(ControlBuilder.CreateDurationPanelForSongs(song.Duration,offset));         // duration

                p.Controls.Add(ControlBuilder.CreateHorizontalRowForSongs(offset));                                        // horizontal row

        
                panelSongsContainer.Controls.Add(p);

              

            });

        }

        private void UpdateSongsPanelBySong(string artist = null)
        {
            Panel pz = (Panel)panelSongsContainer.Controls[UserPreferences.SelectedSongIndex];
            pz.Controls[2].Text = tbEditSongname.Text;
            pz.Controls[3].Text = !string.IsNullOrEmpty(artist) ? artist : tbEditArtist.Text;
            pz.Controls[4].Text = tbEditAlbum.Text;

            tbEditArtist.Text = "";
            PanelEditSongs.Hide();
            panelSongsContainer.Focus();
        }



        private void SongsContextMenu_Opening(object sender, CancelEventArgs e)
        {
            var sourceControl = ((MetroContextMenu)sender).SourceControl;
            UserPreferences.SelectedSong = sourceControl.Tag.ToString();
            UserPreferences.SelectedSongIndex = panelSongsContainer.Controls.IndexOf(sourceControl);
        }






        private Panel CreatePanelForSongs(int y, string songName)
        {

            Panel p = ControlBuilder.CreatePanelForSongs(songName,y);
            p.ContextMenuStrip = SongsContextMenu;


            p.Click += SongPanelClick;
            p.MouseEnter += (s, e) =>
            {
                if (p.BackColor != Color.RoyalBlue)
                    p.BackColor = Color.SlateGray;

                try
                {
                    panelSongsContainer.Controls.Cast<Panel>().First(x => x.BackColor == Color.SlateGray && x != p).BackColor = Color.Transparent;
                }
                catch
                {
                    //ignore
                }
            };
            p.DoubleClick += (s, e) =>
            {
                UserPreferences.SelectedSong = p.Tag.ToString();
                PlaySong(UserPreferences.SelectedSong, UserPreferences.SelectedPlaylist);
            };
            return p;
        }


        public void SongPanelClick(object sender, EventArgs e)
        {
            PanelClick((Panel)sender);
        }

        private void PanelClick(Panel p)
        {
            foreach (Panel i in from Panel i in panelSongsContainer.Controls where i.BackColor == Color.RoyalBlue select i)
            {
                i.BackColor = Color.Transparent;
            }

            p.BackColor = p.BackColor == Color.RoyalBlue ? Color.Transparent : Color.RoyalBlue;

            panelSongsContainer.Focus();
        }

        #endregion


        #region FOOTER || MEDIA PLAYER METHODS


        private void pbRetroceder_Click(object sender, EventArgs e)
        {
            GetPreviousSong();
        }

        private void GetPreviousSong()
        {
            string currentPosition = lbCurrentTime.Text;
            switch (currentPosition)
            {
                case "00:00":
                case "00:01":
                case "00:02":
                    {
                        int count = UserPreferences.GetMusicPlayedCount();
                        if (count > 1)
                        {
                            UserPreferences.SongsToReplay.Add(UserPreferences.GetSongAtIndex(count - 1));
                            UserPreferences.RemoveSongPlayedAtIndex(count - 1); ;
                            string songAlreadyPlayed = UserPreferences.GetSongAtIndex(count - 2).Name;
                            UserPreferences.RemoveSongPlayedAtIndex(count - 2);
                            PlaySong(songAlreadyPlayed, UserPreferences.SelectedPlaylist);
                        }
                        break;
                    }

                default:
                    MusicPlayer.Replay();
                    _songPositionSlider.Value = 0;
                    UpdateUserInterfaceControls();
                    break;
            }
        }

        private void pbAvancar_Click(object sender, EventArgs e)
        {
            GetNextSong();
        }

        private void GetNextSong()
        {
            if (string.IsNullOrEmpty(MusicPlayer.CurrentSongName) || string.IsNullOrEmpty(MusicPlayer.CurrentPlaylistName))
                return;

            string nextsong = TmcPlayer.GetNextSong(MusicPlayer.CurrentPlaylistName, MusicPlayer.CurrentSongName);

            if (string.IsNullOrEmpty(nextsong))
                return;


            PlaySong(nextsong, MusicPlayer.CurrentPlaylistName);
            UserPreferences.SelectedSong = nextsong;

        }

        private void UpdateUserInterfaceControls()
        {
            bool isFav = TmcPlayer.IsSongFavourite(MusicPlayer.CurrentPlaylistName, MusicPlayer.CurrentSongName);
            pbFav.Image = !isFav ? Resources.WhiteStar : Resources.GoldStar;

            pbPlayButton.Image = MusicPlayer.IsPlaying == MusicPlayer.Status.Playing ? Resources.Pause : Resources.Play;
            lbDuration.Text = MusicPlayer.CurrentSongLength;
            if (lbDuration.Text.Length >= 6)
                lbDuration.Text = lbDuration.Text.Substring(3);

            lbVolume.Text = MusicPlayer.GetSongVolume();
            lbCurrentSong.Text = MusicPlayer.CurrentSongName;
            lbCurrentArtist.Text = MusicPlayer.CurrentArtistName;
        }


        private void CreateSliderPosition()
        {
            _songPositionSlider = ControlBuilder.CreatePositionSlider();

            _songPositionSlider.ValueChanged += (s, e) =>
            {
                try
                {
                    if (_songPositionSlider.Value == 0 || string.IsNullOrEmpty(MusicPlayer.CurrentSongLength))
                    {
                        lbCurrentTime.Text = "00:00";
                        return;
                    }

                    var totalsec = TimeSpan.Parse("00:" + lbDuration.Text).TotalSeconds;
                    var value = totalsec * _songPositionSlider.Value / _songPositionSlider.Maximum;

                    lbCurrentTime.Text = TimeSpan.FromSeconds(value).ToString(@"mm\:ss");

                }
                catch (Exception)
                {
                    _songPositionSlider.Value = 0;
                }
            };
            _songPositionSlider.MouseUp += (s, e) =>
            {
                MusicPlayer.SetCurrentPosition((ulong)_songPositionSlider.Value * 1000);
                TmcPlayer.SongPositionTimer.Start();
                MusicPlayer.IsPlaying = MusicPlayer.Status.Playing;
                UpdateUserInterfaceControls();
                SongTimerTick();
            };
            _songPositionSlider.MouseDown += (s, e) => TmcPlayer.SongPositionTimer.Stop();

            panelSongPosition.Controls.Add(_songPositionSlider);

        }

        private void pbReplay_Click(object sender, EventArgs e)
        {
            UserPreferences.IsRepeat ^= true;
            pbReplay.Image = UserPreferences.IsRepeat ? Resources.ReplayTrue : Resources.Replay;
        }

        private void _SongTimer_Tick(object sender, EventArgs e)
        {
            SongTimerTick();
        }

        private void SongTimerTick()
        {
            if (string.IsNullOrEmpty(MusicPlayer.CurrentPosition()) && MusicPlayer.IsPlaying != MusicPlayer.Status.Paused)
            {
                lbCurrentTime.Text = "00:00";
                return;
            }


            if (lbCurrentTime.Text == lbDuration.Text && lbDuration.Text != "00:00")
            {
                if (UserPreferences.IsRepeat)
                {
                    MusicPlayer.Replay();
                    UpdateUserInterfaceControls();
                    return;
                }

                string songName = TmcPlayer.GetNextSong();

                if (songName != string.Empty)
                {
                    _songPositionSlider.Value = _songPositionSlider.Maximum;
                    Thread.Sleep(400);
                    lbCurrentTime.Text = "00:00";
                    MusicPlayer.Open(TmcPlayer.MusicFolder, songName, UserPreferences.SelectedPlaylist);
                    _songPositionSlider.Value = 0;
                    _songPositionSlider.Maximum = MusicPlayer.GetCurrentSongLengthInSeconds();
                    UpdateUserInterfaceControls();
                }

            }
            else
            {
                try
                {

                    if (string.IsNullOrEmpty(lbDuration.Text))
                        lbDuration.Text = MusicPlayer.CurrentSongLength;

                    if (MusicPlayer.IsPlaying != MusicPlayer.Status.Playing)
                        return;

                    var currentSeconds = TimeSpan.Parse("00:" + lbCurrentTime.Text).TotalSeconds;
                    var totalSec = TimeSpan.Parse("00:" + lbDuration.Text).TotalSeconds;

                    double val = currentSeconds / totalSec;

                    var value = Math.Round(val * _songPositionSlider.Maximum, 0);

                    _songPositionSlider.Value = int.Parse(value.ToString());

                    lbCurrentTime.Text = TimeSpan.FromSeconds(currentSeconds + 1).ToString(@"mm\:ss");


                }
                catch
                {
                   //ignore
                }
            }
        }

        private void PlaySong(string songName = null, string playlist = null, bool forceRestart = false)
        {
            if (!string.IsNullOrEmpty(songName) || forceRestart) // abrir musica nova
            {
                MusicPlayer.Open(TmcPlayer.MusicFolder, songName, playlist);
                lbCurrentTime.Text = "00:00";
                _songPositionSlider.Value = 0;
                _songPositionSlider.Maximum = MusicPlayer.GetCurrentSongLengthInSeconds();
            }


            else // Songname empty, quer dizer que é pause/play
            {
                if (MusicPlayer.IsPlaying == MusicPlayer.Status.Playing) // Pause
                    MusicPlayer.Pause();
                else if (!string.IsNullOrEmpty(MusicPlayer.CurrentSongName)) // Play
                    MusicPlayer.Play(true);
                else // open
                {
                    if (!String.IsNullOrEmpty(UserPreferences.SelectedSong))
                    {
                        MusicPlayer.Open(TmcPlayer.MusicFolder, UserPreferences.SelectedSong,UserPreferences.SelectedPlaylist);
                        lbCurrentTime.Text = "00:00";
                        _songPositionSlider.Value = 0;
                        _songPositionSlider.Maximum = MusicPlayer.GetCurrentSongLengthInSeconds();
                    }

                }
            }
            TmcPlayer.SongPositionTimer.Start();
            UpdateUserInterfaceControls();
        }

        private void pbPlayButton_Click(object sender, EventArgs e)
        {
            PlaySong();
        }


        private void pbFav_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(MusicPlayer.CurrentSongName)) return;

            TmcPlayer.SetSongAsFavourite(MusicPlayer.CurrentSongName, MusicPlayer.CurrentPlaylistName);
            UpdateUserInterfaceControls();
        }

        private void pbShuffle_Click(object sender, EventArgs e)
        {
            UserPreferences.IsShuffle ^= true;
            pbShuffle.Image = UserPreferences.IsShuffle ? Resources.shuffle_on : Resources.Shuffle;
        }


        private void CreateSliderVolume()
        {
           _songVolumeSlider = ControlBuilder.CreateSliderVolume(pbSpeaker.Location.X, pbSpeaker.Location.Y);
            
            _songVolumeSlider.ValueChanged += (s, e) =>
           {
               MusicPlayer.SetSongVolume(_songVolumeSlider.Value);
               lbVolume.Text = _songVolumeSlider.Value.ToString();
           };
            PanelBottomLeftUserControls.Controls.Add(_songVolumeSlider);
        }


        #endregion


        #region ANIMATIONS

        private async void ShowSongPanelCenter()
        {
            while (true)
            {
                int x = panelContainerForUserSongs.Location.X;
                int y = panelContainerForUserSongs.Location.Y;

                x += Math.Abs(x) / 18 + 10;

                if (x > 0)
                    x = 0;

                panelContainerForUserSongs.Location = new Point(x, y);

                if (x == 0) return;

                await Task.Delay(1);
            }
        }

        private async void ShowDownloadMenu()
        {
            while (true)
            {
                int x = panelContainerForUserSongs.Location.X;
                int y = panelContainerForUserSongs.Location.Y;

                x -= Math.Abs(x) / 18 + 10;

                panelContainerForUserSongs.Location = new Point(x, y);
                if (x + panelContainerForUserSongs.Size.Width < 0)
                    return;

                await Task.Delay(1);

            }
        }

        private void panelSearch_Click(object sender, EventArgs e)
        {
            ShowDownloadMenu();

            foreach (Panel i in panelNavigator.Controls)
                i.Controls[0].Visible = false;

            ((Panel)sender).Controls[0].Visible = true;
        }

        private void panelSongsContainer_MouseEnter(object sender, EventArgs e)
        {
            panelSongsContainer.Focus();
        }

        private void lbAddNewPlaylist_MouseLeave(object sender, EventArgs e)
        {
            ((MetroLabel)sender).ForeColor = Color.Chocolate;
        }

        private void lbAddNewPlaylist_MouseEnter(object sender, EventArgs e)
        {
            ((MetroLabel)sender).ForeColor = Color.OrangeRed;
        }

        private void PanelCenter_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                panelSongsContainer.Controls.Cast<Panel>().First(x => x.BackColor == Color.SlateGray).BackColor = Color.Transparent;
            }
            catch
            {
                //ignore
            }
        }

        private void SongMenu_Click(object sender, EventArgs e)
        {
            ShowSongPanelCenter();

            foreach (Panel i in panelNavigator.Controls)
                i.Controls[0].Visible = false;

            ((Panel)sender).Controls[0].Visible = true;
        }

        private async void lbCurrentSong_MouseHover(object sender, EventArgs e)
        {
            Label s = (Label)sender;
            if (s.Text.Length > 20)
                await ApplyMarqueeEffect(s.Text);

        }

        private async Task ApplyMarqueeEffect(string songName)
        {
            string currentSongName = songName + "     ";
            string marqueeText = currentSongName.Substring(1) + currentSongName.Substring(0, 1);

            while (marqueeText != currentSongName)
            {
                await Task.Delay(140);
                marqueeText = marqueeText.Substring(1) + marqueeText.Substring(0, 1);
                lbCurrentSong.Text = marqueeText;
            }
        }

        #endregion

        #region ToolStripMenu || remove erase add etc etc

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pl = UserPreferences.SelectedPlaylist;
            string song = UserPreferences.SelectedSong;

            if (MetroMessageBox.Show(this, string.Format("Are you sure to remove {0} from {1} ?", song, pl), "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                TmcPlayer.RemoveSong(pl, song);
                PopulateSongsPanelByPlaylist(pl);
            }
        }

        private void eraseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MetroMessageBox.Show(this, "Quer mesmo apagar essa música?", "Apagar música", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes)
            {
                TmcPlayer.EraseSongFromLibrary(UserPreferences.SelectedSong);
                PopulateSongsPanelByPlaylist(UserPreferences.SelectedPlaylist);
            }
        }
        private void tdmEdit_Click(object sender, EventArgs e)
        {
            Song s = TmcPlayer.FindSong(UserPreferences.SelectedPlaylist, UserPreferences.SelectedSong);
            tbEditSongname.Text = s.Name;
            tbEditAlbum.Text = s.Album;
            cbEditArtist.Text = s.Artist;
            PanelEditSongs.Show();
        }

        private void btnApplyChanges_Click(object sender, EventArgs e)
        {
            string artist = (cbEditArtist.Visible) ? cbEditArtist.Text : tbEditArtist.Text;

            if (string.IsNullOrEmpty(UserPreferences.ListOfArtists.Find(x => x.ToString() == artist)))
                UserPreferences.ListOfArtists.Add(artist);

            TmcPlayer.EditSong(UserPreferences.SelectedPlaylist, UserPreferences.SelectedSong, tbEditSongname.Text, artist, tbEditAlbum.Text);

            UpdateSongsPanelBySong(artist);
        }

        private void tsmPlay_Click(object sender, EventArgs e)
        {
            PlaySong(UserPreferences.SelectedSong);
        }


        private void PopulateSongsContextMenuWithPlaylists()
        {
            foreach (var tsi in TmcPlayer.Library.Select(p => new ToolStripMenuItem(p.Name)).Where(tsi => tsi.Text != "All Songs"))
            {
                tsi.Click += (s, e) =>
                {
                    string playlist = ((ToolStripItem)s).Text;
                    TmcPlayer.AddNewSongToPlaylist(playlist, UserPreferences.SelectedSong);
                };
                ((ToolStripDropDownItem)SongsContextMenu.Items[1]).DropDownItems.Add(tsi);
            }
        }

        #endregion

        #region SortMethods

        private void OrderByField(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(UserPreferences.SelectedPlaylist))
                return;

            UserPreferences.SortDirection = UserPreferences.SortDirection == "desc" ? "asc" : "desc";
            Playlist p = TmcPlayer.FindPlaylist(UserPreferences.SelectedPlaylist);

            string field = ((Control)sender).Tag.ToString();
            switch (field)
            {
                case "Favourite":
                    {
                        p.PlayListSongs = UserPreferences.SortDirection == "desc" ? p.PlayListSongs.OrderByDescending(y => y.IsFavourite).ToList() : p.PlayListSongs.OrderBy(y => y.IsFavourite).ToList();
                        break;
                    }

                case "Name":
                    {
                        p.PlayListSongs = UserPreferences.SortDirection == "desc" ? p.PlayListSongs.Where(x => !String.IsNullOrWhiteSpace(x.Name)).OrderByDescending(y => y.Name).ToList() : p.PlayListSongs.OrderBy(y => y.Name).ToList();
                        break;
                    }

                case "Duration":
                    {
                        p.PlayListSongs = UserPreferences.SortDirection == "desc" ? p.PlayListSongs.OrderByDescending(y => TimeSpan.Parse(y.Duration).TotalSeconds).ToList() : p.PlayListSongs.OrderBy(y => TimeSpan.Parse(y.Duration).TotalSeconds).ToList();
                        break;
                    }

                case "Album":
                    {
                        p.PlayListSongs = UserPreferences.SortDirection == "desc" ? p.PlayListSongs.OrderByDescending(y => y.Album).ToList() : p.PlayListSongs.Where(x => !String.IsNullOrWhiteSpace(x.Album)).OrderBy(y => y.Album).ToList();
                        break;
                    }

                case "Artist":
                    {
                        p.PlayListSongs = UserPreferences.SortDirection == "desc" ? p.PlayListSongs.Where(x => !String.IsNullOrWhiteSpace(x.Artist)).OrderByDescending(y => y.Artist).ToList() : p.PlayListSongs.Where(x => !String.IsNullOrWhiteSpace(x.Artist)).OrderBy(y => y.Artist).ToList();
                        break;
                    }
            }

            PopulateSongsPanelByPlaylist(null, p);
        }


        #endregion


        private void pbAddArtist_Click(object sender, EventArgs e)
        {
            if (tbEditArtist.Visible)
            {
                cbEditArtist.Show();
                tbEditArtist.Hide();
            }
            else
            {
                cbEditArtist.Hide();
                tbEditArtist.Show();
            }
        }

        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pl = sender.GetType() == typeof(Label) ? ((Label)sender).Text : UserPreferences.SelectedContextMenuPlaylist;

            string song = TmcPlayer.PlayNewPlaylist(pl);

            if (string.IsNullOrEmpty(song))
                MetroMessageBox.Show(this, "Não tem músicas na sua playlist!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                PlayPlaylist(song, pl);

        }



        private void PlayPlaylist(string song,string pl)
        {
           
            PlaySong(song, pl, true);
            UserPreferences.SongsToReplay.Clear();
            UserPreferences.SongsPlayed.Clear();
            panelSongsContainer.Focus();
        }

        private void removePlaylistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (UserPreferences.SelectedContextMenuPlaylist == "All Songs")
                return;

            if (UserPreferences.SelectedContextMenuPlaylist == MusicPlayer.CurrentPlaylistName)
            {
                MetroMessageBox.Show(this, "Não pode remover a playlist que está a tocar!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MetroMessageBox.Show(this, "Do you wish to remove " + UserPreferences.SelectedContextMenuPlaylist + " ?", "Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                TmcPlayer.RemovePlayList(UserPreferences.SelectedContextMenuPlaylist);
                PopulatePlaylistPanel();
            }
        }

        private void PlaylistContextMenu_Opening(object sender, CancelEventArgs e)
        {
            var sourceControl = ((MetroContextMenu)sender).SourceControl;
            UserPreferences.SelectedContextMenuPlaylist = sourceControl.Tag.ToString();
        }

        private void PanelMainTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MainWindowForm_Resize(object sender, EventArgs e)
        {



        }

        private void MainWindowForm_SizeChanged(object sender, EventArgs e)
        {

        }

        private void MainWindowForm_ClientSizeChanged(object sender, EventArgs e)
        {

        }

        private void panelSongsContainer_Paint(object sender, PaintEventArgs e)
        {

        }





    }
}