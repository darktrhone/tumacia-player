﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Microsoft.WindowsAPICodePack.Shell;

namespace WindowsFormsApplication2
{
    internal class FileHandler
    {
        #region CREATE FOLDERS

        public static string CreateMusicFolder()
        {
            string specificFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "TMC Player");

            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            specificFolder = Path.Combine(specificFolder, "Music");

            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            return specificFolder;
        }

        public static string CreateDownloadsFolder()
        {
            string specificFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "TMC Player");

            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            specificFolder = Path.Combine(specificFolder, "Downloads");

            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            return specificFolder;
        }
        public static string CreateDataFolder()
        {
            string specificFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "TMC Player");

            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            specificFolder = Path.Combine(specificFolder, "Data");

            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);

            return specificFolder;
        }

        #endregion

        #region SongDuration

        public static string GetSongDuration(string file, string folder)
        {
            ShellFile so = ShellFile.FromFilePath(Path.Combine(folder, file));
            double nanoseconds;
            Double.TryParse(so.Properties.System.Media.Duration.Value.ToString(), out nanoseconds);

            if (nanoseconds <= 0)
                return string.Empty;

            //convert to miliseconds
            nanoseconds = (nanoseconds * 0.0001) / 1000;

            TimeSpan ts = TimeSpan.FromSeconds(Convert.ToSingle(nanoseconds));

            return ts.ToString(@"mm\:ss");
        }


        #endregion


        #region FILE TREATMENT

        public static string GetCleanFileNameAndLengthByUrl(string url, string filename, out long length)
        {
            length = 0;
            try
            {
                WebRequest wr = WebRequest.Create(url);
                wr.Timeout = 1000;
                WebResponse wrsp = wr.GetResponse();
                length = long.Parse(wrsp.Headers["Content-Length"]);
                wrsp.Dispose();


                filename = RemoveDiacritics(filename);
                filename = Path.GetInvalidFileNameChars().Aggregate(filename, (current, c) => current.Replace(c.ToString(), string.Empty));

                filename = filename.Replace(".mp3", "");
                filename = filename.Replace(".", "");

                return filename;

            }
            catch
            {
                return string.Empty;
            }
        }

        static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in from c in normalizedString let unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c) where unicodeCategory != UnicodeCategory.NonSpacingMark select c)
            {
                stringBuilder.Append(c);
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }




        #endregion

        #region GENERATE XML
        internal static bool SavePlaylist(List<Playlist> listPl, string path)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement playlistRoot = doc.CreateElement("Playlists");

            listPl.ForEach(pl => playlistRoot.AppendChild(GenerateXmlPlaylist(pl, doc)));

            doc.AppendChild(playlistRoot);

            doc.Save(Path.Combine(path, "Playlists.xml"));

            return true;
        }

        internal static XmlElement GenerateXmlElement(string name, string value, XmlDocument doc)
        {
            XmlElement x = doc.CreateElement(name);
            x.InnerText = value;
            return x;
        }

        internal static XmlElement GenerateXmlSong(Song s, XmlDocument doc)
        {
            XmlElement song = doc.CreateElement("Song");

            song.AppendChild(GenerateXmlElement("Name", s.Name, doc));
            song.AppendChild(GenerateXmlElement("Album", s.Album, doc));
            song.AppendChild(GenerateXmlElement("Duration", s.Duration, doc));
            song.AppendChild(GenerateXmlElement("Artist", s.Artist, doc));
            song.AppendChild(GenerateXmlElement("Favourite", s.IsFavourite.ToString(), doc));
            return song;

        }

        internal static XmlElement GenerateXmlPlaylist(Playlist p, XmlDocument doc)
        {
            XmlElement playlistElement = doc.CreateElement("Playlist");
            XmlElement songsContainer = doc.CreateElement("Songs");


            p.PlayListSongs.ForEach(s => songsContainer.AppendChild(GenerateXmlSong(s, doc)));

            playlistElement.AppendChild(GenerateXmlElement("Name", p.Name, doc));
            playlistElement.AppendChild(songsContainer);
            return playlistElement;
        }
        #endregion

        #region POPULATE FROM XML
        internal static List<Playlist> PopulateLibraryFromXml(string filePath)
        {
            try
            {
                if (new FileInfo(filePath).Exists)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath);

                    if (doc.HasChildNodes)
                        return PopulateLibraryFromXml(doc);
                }
                else
                {
                    return new List<Playlist>();
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possivel carregar as suas playlist!");
                return null;
            }

            return null;
        }

        internal static List<Playlist> PopulateLibraryFromXml(XmlDocument doc)
        {

            if (doc.GetElementsByTagName("Playlist").Count == 0)
                return new List<Playlist>();

            return (from XmlNode playlistNode
                    in doc.GetElementsByTagName("Playlist")
                    select PopulatePlaylistFromXml(playlistNode))
                    .ToList();

        }

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        internal static Playlist PopulatePlaylistFromXml(XmlNode playlistNode)
        {

            if (!(playlistNode != null && playlistNode.SelectSingleNode("Name") != null))
                return null;

            Playlist pl = new Playlist(playlistNode.SelectSingleNode("Name").InnerText);

            if (!(playlistNode.SelectSingleNode("Songs") != null && playlistNode.SelectSingleNode("Songs").SelectNodes("Song") != null))
                return pl;

            XmlNodeList songList = playlistNode.SelectSingleNode("Songs").SelectNodes("Song");

            if (!(songList != null && songList.Count > 0))
                return pl;

            foreach (XmlNode songNode in songList)
                pl.PlayListSongs.Add(PopulateSongFromXml(songNode));

            return pl;

        }

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        internal static Song PopulateSongFromXml(XmlNode songNode)
        {
            if (!(songNode != null && songNode.SelectSingleNode("Name") != null && songNode.SelectSingleNode("Album") != null && songNode.SelectSingleNode("Artist") != null && songNode.SelectSingleNode("Duration") != null))
                return null;


            return new Song(songNode.SelectSingleNode("Name").InnerText,
                songNode.SelectSingleNode("Album").InnerText,
                songNode.SelectSingleNode("Artist").InnerText,
                songNode.SelectSingleNode("Duration").InnerText,
                songNode.SelectSingleNode("Favourite").InnerText);


        }
        #endregion

        #region POPULATE WITH ALL SONGS
        public static Playlist PopulateWithAllSongs(string name, string path)
        {

            Playlist allSongs = new Playlist(name);

            foreach (Song s in from playlist
                            in TmcPlayer.Library
                            where playlist.Name != name
                            from s in playlist.PlayListSongs
                            where !allSongs.PlayListSongs.Exists(a => a.Name == s.Name)
                            select s)
                            allSongs.PlayListSongs.Add(s);


            try
            {
                new DirectoryInfo(path).GetFiles("*.mp3").ToList().ForEach(f =>
                {

                    if (!allSongs.PlayListSongs.Exists(a => a.Name == f.Name.Replace(".mp3", "")))
                    {
                        string duration = GetSongDuration(f.Name, TmcPlayer.MusicFolder);
                        allSongs.PlayListSongs.Add(new Song(f.Name.Replace(".mp3", ""), duration));

                    }
                });
            }
            catch (Exception)
            {
                MessageBox.Show("Erro na populacao de todas as musicas");
            }
            return allSongs;
         }
        #endregion


    }
}





