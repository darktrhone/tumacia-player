﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace WindowsFormsApplication2
{
    partial class DownloadManagerWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvDownloadDetails = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAddSong = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.wb = new System.Windows.Forms.WebBrowser();
            this.cbPlaylists = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbxSearch = new System.Windows.Forms.TextBox();
            this.tbAlbum = new System.Windows.Forms.TextBox();
            this.tbSongname = new System.Windows.Forms.TextBox();
            this.cbArtist = new MetroFramework.Controls.MetroComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbNewArtist = new System.Windows.Forms.PictureBox();
            this.tbArtist = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNewArtist)).BeginInit();
            this.SuspendLayout();
            // 
            // lvDownloadDetails
            // 
            this.lvDownloadDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvDownloadDetails.FullRowSelect = true;
            this.lvDownloadDetails.HideSelection = false;
            this.lvDownloadDetails.Location = new System.Drawing.Point(3, 3);
            this.lvDownloadDetails.Name = "lvDownloadDetails";
            this.lvDownloadDetails.Size = new System.Drawing.Size(305, 483);
            this.lvDownloadDetails.TabIndex = 2;
            this.lvDownloadDetails.UseCompatibleStateImageBehavior = false;
            this.lvDownloadDetails.View = System.Windows.Forms.View.Details;
            this.lvDownloadDetails.Click += new System.EventHandler(this.lvDownloadDetails_Click);
            this.lvDownloadDetails.DoubleClick += new System.EventHandler(this.lvDownloadDetails_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 162;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "%";
            this.columnHeader2.Width = 44;
            // 
            // btnAddSong
            // 
            this.btnAddSong.Location = new System.Drawing.Point(630, 269);
            this.btnAddSong.Name = "btnAddSong";
            this.btnAddSong.Size = new System.Drawing.Size(75, 23);
            this.btnAddSong.TabIndex = 10;
            this.btnAddSong.Text = "Add";
            this.btnAddSong.UseVisualStyleBackColor = true;
            this.btnAddSong.Click += new System.EventHandler(this.btnAddSong_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(905, 38);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 22;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btDownload_Click);
            // 
            // wb
            // 
            this.wb.Location = new System.Drawing.Point(824, 425);
            this.wb.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb.Name = "wb";
            this.wb.ScriptErrorsSuppressed = true;
            this.wb.Size = new System.Drawing.Size(20, 20);
            this.wb.TabIndex = 23;
            this.wb.Url = new System.Uri("https://www.youtube2mp3.cc", System.UriKind.Absolute);
            this.wb.Visible = false;
            // 
            // cbPlaylists
            // 
            this.cbPlaylists.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.cbPlaylists.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbPlaylists.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cbPlaylists.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.cbPlaylists.FormattingEnabled = true;
            this.cbPlaylists.IntegralHeight = false;
            this.cbPlaylists.ItemHeight = 19;
            this.cbPlaylists.Location = new System.Drawing.Point(486, 115);
            this.cbPlaylists.MaximumSize = new System.Drawing.Size(150, 0);
            this.cbPlaylists.MaxLength = 15;
            this.cbPlaylists.MinimumSize = new System.Drawing.Size(20, 0);
            this.cbPlaylists.Name = "cbPlaylists";
            this.cbPlaylists.Size = new System.Drawing.Size(150, 25);
            this.cbPlaylists.Sorted = true;
            this.cbPlaylists.Style = MetroFramework.MetroColorStyle.Orange;
            this.cbPlaylists.TabIndex = 30;
            this.cbPlaylists.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cbPlaylists.UseCustomBackColor = true;
            this.cbPlaylists.UseCustomForeColor = true;
            this.cbPlaylists.UseSelectable = true;
            this.cbPlaylists.UseStyleColors = true;
            this.cbPlaylists.Click += new System.EventHandler(this.cbPlaylists_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(483, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 16);
            this.label1.TabIndex = 31;
            this.label1.Text = "Playlist";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(691, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 32;
            this.label2.Text = "Artist";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(491, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 16);
            this.label3.TabIndex = 33;
            this.label3.Text = "Album";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(691, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 16);
            this.label4.TabIndex = 34;
            this.label4.Text = "Songname";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(491, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 35;
            this.label5.Text = "Youtube Url";
            // 
            // txtbxSearch
            // 
            this.txtbxSearch.Location = new System.Drawing.Point(486, 38);
            this.txtbxSearch.Name = "txtbxSearch";
            this.txtbxSearch.Size = new System.Drawing.Size(358, 20);
            this.txtbxSearch.TabIndex = 36;
            // 
            // tbAlbum
            // 
            this.tbAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAlbum.Location = new System.Drawing.Point(490, 195);
            this.tbAlbum.Name = "tbAlbum";
            this.tbAlbum.Size = new System.Drawing.Size(146, 22);
            this.tbAlbum.TabIndex = 37;
            this.tbAlbum.TextChanged += new System.EventHandler(this.tbAlbum_TextChanged);
            // 
            // tbSongname
            // 
            this.tbSongname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSongname.HideSelection = false;
            this.tbSongname.Location = new System.Drawing.Point(694, 195);
            this.tbSongname.Name = "tbSongname";
            this.tbSongname.Size = new System.Drawing.Size(150, 22);
            this.tbSongname.TabIndex = 38;
            // 
            // cbArtist
            // 
            this.cbArtist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.cbArtist.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbArtist.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cbArtist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.cbArtist.FormattingEnabled = true;
            this.cbArtist.IntegralHeight = false;
            this.cbArtist.ItemHeight = 19;
            this.cbArtist.Location = new System.Drawing.Point(694, 115);
            this.cbArtist.MaximumSize = new System.Drawing.Size(150, 0);
            this.cbArtist.MaxLength = 15;
            this.cbArtist.MinimumSize = new System.Drawing.Size(20, 0);
            this.cbArtist.Name = "cbArtist";
            this.cbArtist.Size = new System.Drawing.Size(150, 25);
            this.cbArtist.Sorted = true;
            this.cbArtist.Style = MetroFramework.MetroColorStyle.Orange;
            this.cbArtist.TabIndex = 39;
            this.cbArtist.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cbArtist.UseCustomBackColor = true;
            this.cbArtist.UseCustomForeColor = true;
            this.cbArtist.UseSelectable = true;
            this.cbArtist.UseStyleColors = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WindowsFormsApplication2.Properties.Resources.swap_red;
            this.pictureBox1.Location = new System.Drawing.Point(804, 152);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pbNewArtist
            // 
            this.pbNewArtist.Image = global::WindowsFormsApplication2.Properties.Resources.orange_add_new;
            this.pbNewArtist.Location = new System.Drawing.Point(850, 114);
            this.pbNewArtist.Name = "pbNewArtist";
            this.pbNewArtist.Size = new System.Drawing.Size(26, 26);
            this.pbNewArtist.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbNewArtist.TabIndex = 41;
            this.pbNewArtist.TabStop = false;
            this.pbNewArtist.Click += new System.EventHandler(this.pbNewArtist_Click);
            // 
            // tbArtist
            // 
            this.tbArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbArtist.Location = new System.Drawing.Point(694, 115);
            this.tbArtist.Name = "tbArtist";
            this.tbArtist.Size = new System.Drawing.Size(150, 22);
            this.tbArtist.TabIndex = 42;
            this.tbArtist.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(314, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Status";
            this.label6.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblStatus.Location = new System.Drawing.Point(357, 43);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 44;
            this.lblStatus.Text = "Status";
            this.lblStatus.Visible = false;
            // 
            // DownloadManagerWindow
            // 
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbArtist);
            this.Controls.Add(this.pbNewArtist);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cbArtist);
            this.Controls.Add(this.tbSongname);
            this.Controls.Add(this.tbAlbum);
            this.Controls.Add(this.txtbxSearch);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPlaylists);
            this.Controls.Add(this.wb);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnAddSong);
            this.Controls.Add(this.lvDownloadDetails);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "DownloadManagerWindow";
            this.Size = new System.Drawing.Size(1024, 489);
            this.Load += new System.EventHandler(this.DownloadManagerWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNewArtist)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListView lvDownloadDetails;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private Button btnAddSong;
        private Button btnDownload;
        private WebBrowser wb;
        private MetroComboBox cbPlaylists;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private TextBox txtbxSearch;
        private TextBox tbAlbum;
        private TextBox tbSongname;
        private MetroComboBox cbArtist;
        private PictureBox pictureBox1;
        private PictureBox pbNewArtist;
        private TextBox tbArtist;
        private Label label6;
        private Label lblStatus;
    }
}
