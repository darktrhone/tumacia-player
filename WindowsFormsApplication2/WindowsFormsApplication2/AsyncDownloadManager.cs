﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public static class AsyncDownloadManager
    {

        static AsyncDownloadManager()
        {
            IsBusy = false;
            FilesColletion = new List<FileObject>();
           
        }

        public static List<FileObject> FilesColletion;

        public static WebClient Instance;

        public static string DownloadFolder { get; set; }

        public static bool IsBusy { get; set; }


        public static async Task ProcessAllDownloads()
        {

            if (IsBusy || Instance != null)
            {
                await Task.Delay(500);
            }
            else
            {
                try
                {
                    IsBusy = true;
                    Instance = new WebClient();
                    FileObject fo = FilesColletion.FirstOrDefault(x => x.Status == 0);

                    if (fo == null)
                        return;


                    string fullPath = Path.Combine(DownloadFolder, fo.FileName + ".mp3");

                    await Instance.DownloadFileTaskAsync(new Uri(fo.Url), fullPath);

                    FilesColletion.Find(x => x == fo).Status = -1;

                    TmcPlayer.AddNewSongToPlaylist("All Songs", fo.FileName);

                    Instance.Dispose();
                    Instance = null;
                    IsBusy = false;

                    if (FilesColletion.FirstOrDefault(x => x.Status == 0) != null)
                        ProcessAllDownloads();




                }
                catch (Exception)
                {
                    MessageBox.Show("erroooo3o");
                    throw;
                }
            }

        }

        public static string AddNewUrlToProcess(string url, string filename)
        {
            long fLength;

            if (FileIsInQueueOrProcessed(url))
                return string.Empty;

            filename = FileHandler.GetCleanFileNameAndLengthByUrl(url, filename, out fLength);
            if (filename == string.Empty)
                return string.Empty;


            FileObject fo = new FileObject { Url = url, Length = fLength, Status = 0, FileName = filename };

            FilesColletion.Add(fo);

            return filename;
        }


        public static bool FileIsInQueueOrProcessed(string url)
        {
          
            return FilesColletion.Exists(f => f.Url == url);


        }

        public static string GetFileNameByUrl(string url)
        {
            return FilesColletion.Find(z => z.Url == url).FileName;
        }



    }






    public class FileObject
    {
        public string Url { get; set; }
        public string FileName { get; set; }
        public long Length { get; set; }
        public int Status { get; set; }

    }
}
